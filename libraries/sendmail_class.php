<?php

class SendMaillist  {

	public $url_back, $url_post, $url_post_add;

	function __construct()
	{
	
		global $base_url;
	
		$this->url_back=''; //set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule']));
		
		$this->url_post=''; // set_admin_link( 'maillist', array('op' => 3, 'IdModule' => $_GET['IdModule']));
	
		$this->url_post_add='';
		$this->url_post_del='';
		
		$this->check_mailer();
	
	}
	
	static public function get_modules_array()
	{
	
		global $base_path, $lang;
		
		$arr_modules_mail=array(0, $lang['maillist']['no_module'], '');
		$arr_def_modules_mail=array();
						
		//Load modules array...
		
		$handle=opendir($base_path."modules/maillist/modules");

		while ($file = readdir($handle))
		{
			if(preg_match('/.*\.php$/', $file))
			{
			
				$file_module=str_replace('.php', '', $file);
			
				$arr_modules_mail[]=ucfirst($file_module);
				$arr_modules_mail[]=$file_module;
				$arr_def_modules_mail[]=$file_module;
				$model['maillist']->components['module']->arr_values[]=$file_module;

			}
		}
		
		return array($arr_modules_mail, $arr_def_modules_mail);
	
	}
	
	public function show_form()
	{
	
		global $lang, $model, $user_data;
		
		settype($_GET['iddraft'], 'integer');
		
		$arr_draft['message']='';
		$arr_draft['title']='';
				
		if($_GET['iddraft']>0)
		{
		
			$arr_draft=$model['draft_email']->select_a_row($_GET['iddraft']);
			
			settype($arr_draft['idlist'], 'integer');
			
			$_GET['idlist']=$arr_draft['idlist'];
		
		}
	
		settype($_GET['idlist'], 'integer');
		
		$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array(), true);
		
		list($idlist, $name_list, $table_related, $field_related, $module_default)=webtsys_fetch_row($query);
		
		settype($idlist, 'integer');
		
		$arr_name_list=array();
		$arr_name_list[$idlist]='';
		$arr_name_list[0]='';
		
		list($arr_modules_mail, $arr_def_modules_mail)=SendMaillist::get_modules_array();
		
		ob_start();
		
		if($idlist>0)
		{
		
			$arr_name_list[$idlist]=$name_list;
			$arr_name_list[0]=$lang['common']['registered_users'];
			
			?>
			<form method="post" action="<?php echo $this->url_post; ?>" enctype="multipart/form-data">
			<?php echo set_csrf_key(); ?>
			<div class="form">
				<p><label for="type"><?php echo $lang['maillist']['send_owner_mail']; ?>:</label><select name="send_trial"><option value="0"><?php echo $lang['common']['no']; ?></option><option value="1"><?php echo $lang['common']['yes']; ?></option></select> &nbsp;<?php echo $user_data['email']; ?></p>
				<p><label for="type"><?php echo $lang['common']['subject']; ?>:</label><input type="text" name="title" value="<?php echo $arr_draft['title']; ?>"/></p>
				<p><label for="type"><?php echo $lang['common']['text']; ?>:</label><?php echo TextAreaBBForm('message', '', $arr_draft['message']); ?></p>
				<!--<p>
				<label for="type"><?php echo $lang['maillist']['mail_type']; ?>:</label><select name="type">
					<option value="0"><?php echo $lang['common']['text']; ?></option>
					<option value="1"><?php echo $lang['common']['html']; ?></option>
				</select></p>-->
				<p>
				<label for="module_mail"><?php echo $lang['maillist']['mail_module']; ?>:</label>
				<?php
				
					$arr_modules_mail[0]=$module_default;
				
					echo SelectForm('module_mail', '', $arr_modules_mail);
				
				?>
				</p>
				<p>
				<label for="attachment"><?php echo $lang['maillist']['attachment']; ?>: </label><input type="file" name="attachment[]" />
				</p>
				<p><input type="submit" value="<?php echo $lang['maillist']['send_mail']; ?>" /></p>
				<input type="hidden" name="idlist" value="<?php echo $idlist; ?>" />
			</div>
			</form>
			<?php
			
		}
		
		$cont_admin=ob_get_contents();

		ob_end_clean();
		
		echo load_view(array($lang['maillist']['send_mail_list'].' - '.$arr_name_list[$idlist], $cont_admin), 'content');
		
		//echo '<a href="'.$this->url_back.'">'.$lang['common']['go_back'].'</a>';
	
	}
	
	public function send_mail()
	{
	
		global $model, $lang, $user_data, $base_path;
	
		$errno=0;
		$errstr='';

		settype($_POST['html'], 'integer');
		settype($_POST['send_trial'], 'integer');
		settype($_POST['idlist'], 'integer');
		settype($_POST['module_mail'], 'string');

		$_POST['module_mail']=form_text($_POST['module_mail']);
		
		$post['title']=$_POST['title'];
		$post['message']=$_POST['message'];
		$post['type']=1;//$_POST['type'];
		$post['module']=$_POST['module_mail'];
		$post['iduser']=$user_data['IdUser'];
		$post['token']=$user_data['key_connection'];
		$post['idlist']=$_POST['idlist'];
		$post['trial']=$_POST['send_trial'];
		$post['attachment']=array();
		$post['unique_id']=get_token();
		
		list($arr_modules_mail, $arr_def_modules_mail)=SendMaillist::get_modules_array();
		
		$model['maillist']->components['module']->arr_values=$arr_modules_mail;
		
		$query=$model['maillist_name']->select('where IdMaillist_name='.$post['idlist'], array());
		
		list($idlist, $name_list, $table_related, $field_related, $module_default)=webtsys_fetch_row($query);
		
		settype($idlist, 'integer');
		
		$arr_name_list[$idlist]=$name_list;
		$arr_name_list[0]=$lang['common']['registered_users'];
		
		//print_r($post);
		
		$num_list=$model['maillist']->select_count('where finished=0 and idlist='.$idlist, 'IdMaillist');
		
		$idmaillist=0;

		$yes_mail=0;
		
		//Check if can boot daemon
		
		if(!is_writable($base_path.'/modules/maillist/log/'))
		{
		
			echo 'Cannot write log: check permissions on log folder: '.$base_path.'/modules/maillist/log/mail.log';
					
			die;
		
		}
		
		//Check files
		
		if($_FILES['attachment']['name'][0]!='')
		{
			foreach($_FILES['attachment']['name'] as $key_attachment => $attachment)
			{
			
				//Move the file, update
				
				
				$path_file=$base_path.'modules/maillist/attachments/'.$post['unique_id'].'/';
				
				if(mkdir($path_file))
				{
					$name_file=$attachment;
					
					if(!move_uploaded_file ( $_FILES['attachment']['tmp_name'][$key_attachment], $path_file.$name_file ) )
					{
					
						unlink($path_file);
					
						echo 'Cannot move uploaded file: check permissions on attachments folder';
					
						die;
					
					}
					else
					{
					
						$post['attachment'][]=$path_file.$name_file;
					
					}
					
				}
				else
				{
				
					echo 'Cannot create file folder for this attachment: check permissions on attachments folder';
					
					die;
				
				}
			
			}
			
		}
		
		if($num_list==0)
		{
			if($query=$model['maillist']->insert($post))
			{
				$idmaillist=webtsys_insert_id();
				$yes_mail=1;
			}
			else
			{

				echo '<p>'.$lang['common']['error'].'</p>';

			}

		}
		else
		{

			echo '<p>'.$lang['maillist']['mail_in_queue'].'</p>';

		}
		
		//$pidof=system("pidof /usr/bin/php '.$base_path.'modules/maillist/daemon/daemon.php");
		
		$pidof=system("pidof /usr/bin/php cli.php");
		
		settype($pidof, 'integer');
		
		if($idmaillist>0)
		{
		
			if($yes_mail==1)
			{

				echo '<p>'.$lang['maillist']['loading_daemon_mail'].'</p>';

				$retval=0;
				chdir($base_path);
				$return_value=system('/usr/bin/php cli.php -m maillist -c daemon --maillist_id '.$idlist.' > '.$base_path.'/modules/maillist/log/mail.log &', $retval);
				
				if(DEBUG==1)
				{
					echo '/usr/bin/php cli.php -m maillist -c daemon --maillist_id '.$idlist.' >& '.$base_path.'/modules/maillist/log/mail.log &';
					
					echo '<p>Valor de retorno de demonio: '.$retval.'</p>';
					
					echo '<p>Texto de retorno demonio: '.$return_value.'</p>';
					
				}
				//echo $return_value;
				
				//echo '/usr/bin/php cli.php -m maillist -c daemon --maillist_id '.$idlist.' > /tmp/log &';
				
				if($retval!=0)
				{
		
					echo '<p>'.$lang['maillist']['error_daemon_no_loading'].': '.$retval.'</p>';
		
				}
				else
				{

					echo '<p>'.$lang['maillist']['sending_mail_by_daemon'].'</p>';

				}

			}
			else if($yes_mail==1)
			{

				echo '<p>'.$lang['maillist']['daemon_sending_list_wait'].'</p>';
		
			}
			
		}
		
		$cont_admin=ob_get_contents();

		ob_end_clean();
		
		echo load_view(array($lang['maillist']['send_mail_list'].' - '.$arr_name_list[$idlist], $cont_admin), 'content');

		echo '<a href="'.$this->url_back.'">'.$lang['common']['go_back'].'</a>';
	
	}
	
	public function add_mass_email()
	{
	
		global $model, $lang, $base_url;
	
		settype($_GET['op_add_mail'], 'integer');
		
		settype($_GET['idlist'], 'integer');
			
		$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array('IdMaillist_name', 'name'));
		
		list($idlist, $name_list)=webtsys_fetch_row($query);
		
		settype($idlist, 'integer');
		
		if($idlist>0)
		{
		
			switch($_GET['op_add_mail'])
			{
			
				default:
				
					$arr_forms['mass_email']=new ModelForm('mass_form', 'mass_email', 'FileForm', $lang['maillist']['mass_emails'], 'FileField', $required=1, $parameters='');
					
					$url_post=add_extra_fancy_url($this->url_post_add, array('op_add_mail' => 1));
					
					echo load_view(array($arr_forms, array('mass_email'), $url_post, 'enctype="multipart/form-data"'), 'common/forms/updatemodelform');
				
					$cont_form=ob_get_contents();
				
					ob_end_clean();
					
					echo load_view(array($lang['maillist']['proccess_mass_email'].' - '.$name_list, $cont_form), 'content');
					
					echo '<a href="'.$this->url_back.'">'.$lang['common']['go_back'].'</a>';
				
				break;
				
				case 1:
				
					ob_end_flush();
				
				
					if($idlist>0)
					{
				
						//$emails=trim(file_get_contents($_FILES['mass_email']['tmp_name']));
						
						$arr_emails=file($_FILES['mass_email']['tmp_name']);//explode("\n", $emails);
						
						//print_r($arr_emails);
						
						$good=0;
						$error=0;
						
						//$email_check=new MailField();
						
						//0 email 1 name 2 company 3 job
						
						foreach($arr_emails as $key_email => $str_email)
						{
						
							$arr_email=explode('|', $str_email);
							
							settype($arr_email[0], 'string');
							settype($arr_email[1], 'string');
							settype($arr_email[2], 'string');
							settype($arr_email[3], 'string');
							
							$post=array('email' => $arr_email[0], 'name' => $arr_email[1], 'company' => $arr_email[2], 'job' => $arr_email[3], 'idlist' => $idlist, 'activation' => 1);
								
							if($model['maillist_email']->insert($post))
							{
							
								$good++;
							
							}
							else
							{
							
								$error++;
							
							}
						
						}
						
						$cont_final='<p>'.$lang['maillist']['mass_email_processing_success'].'</p>';
						$cont_final.='<p>'.$lang['maillist']['mass_email_accepted'].': '.$good.'</p>';
						$cont_final.='<p>'.$lang['maillist']['mass_email_rejected'].': '.$error.'</p>';
						
						echo load_view(array($lang['maillist']['mass_email_processing'].' - '.$name_list, $cont_final), 'content');
						
					}
					
					echo '<a href="'.$this->url_back.'">'.$lang['common']['go_back'].'</a>';
				
				break;
			
			}
			
		}
	
	}
	
	public function delete_mass_email()
	{
	
		global $model, $lang, $base_url;
	
		settype($_GET['idlist'], 'integer');
		settype($_GET['op_del_mail'], 'integer');
			
		$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array('IdMaillist_name', 'name'));
		
		list($idlist, $name_list)=webtsys_fetch_row($query);
		
		settype($idlist, 'integer');
		
		if($idlist>0)
		{
			switch($_GET['op_del_mail'])
			{
			
				default:
				
					$arr_forms['mass_email']=new ModelForm('mass_form', 'mass_email', 'FileForm', $lang['maillist']['mass_emails'], 'FileField', $required=1, $parameters='');
				
					$url_post=$url_post=add_extra_fancy_url($this->url_post_del, array('op_del_mail' => 1));;//set_admin_link( 'maillist', array('op' => 7, 'IdModule' => $_GET['IdModule'], 'idlist' => $_GET['idlist']));
					
					echo load_view(array($arr_forms, array('mass_email'), $url_post, 'enctype="multipart/form-data"'), 'common/forms/updatemodelform');
				
					$cont_form=ob_get_contents();
				
					ob_end_clean();
					
					echo load_view(array($lang['maillist']['delete_mass_email'].' - '.$name_list, $cont_form), 'content');
					
					echo '<a href="'.$this->url_back.'">'.$lang['common']['go_back'].'</a>';
				
				break;
				
				case 1:
				
					ob_end_flush();
			
					$emails=trim(file_get_contents($_FILES['mass_email']['tmp_name']));
					
					$arr_emails=explode("\n", $emails);
					
					//print_r($arr_emails);
					
					$good=0;
					$error=0;
					
					foreach($arr_emails as $key_email => $email)
					{
					
						$email_check=$model['maillist_email']->components['email']->check($email);
						
						if($email_check!='')
						{
						
							$query=$model['maillist_email']->delete('where idlist='.$idlist.' and email="'.$email_check.'"');
							
							$good++;
							
						}
						else
						{
						
							$error++;
						
						}
					}
					
					$cont_final='<p>'.$lang['maillist']['mass_email_deleting_success'].'</p>';
					$cont_final.='<p>'.$lang['maillist']['mass_email_accepted'].': '.$good.'</p>';
					$cont_final.='<p>'.$lang['maillist']['mass_email_rejected'].': '.$error.'</p>';
					
					echo load_view(array($lang['maillist']['mass_email_deleting'].' - '.$name_list, $cont_final), 'content');
					
					echo '<a href="'.$this->url_back.'">'.$lang['common']['go_back'].'</a>';
				
				
				break;
			
			}
		}
	
	}
	
	public function export_csv($idlist)
	{
	
		global $model;
		
		$arr_maillist=$model['maillist_name']->select_a_row($idlist, array('IdMaillist_name', 'name'));
	
		settype($arr_maillist['IdMaillist_name'], 'integer');
		
		if($arr_maillist['IdMaillist_name']>0)
		{
		
			$title_file=slugify($arr_maillist['name']);
	
			header('Content-type: text/csv');
			header('Content-disposition: attachment;filename='.$title_file.'.csv');
			
			$query=$model['maillist_email']->select('where idlist='.$idlist, array('email', 'name', 'company', 'job', 'date'));
			
			while($arr_list=webtsys_fetch_array($query))
			{
			
				//echo implode('|', $arr_list)."\n";
				echo $arr_list['email'].'|'.$arr_list['name'].'|'.$arr_list['company'].'|'.$arr_list['job'].'|'.$model['maillist_email']->components['date']->show_formatted($arr_list['date'])."\n";
			
			}
			
		}
	
	}
	
	private function check_mailer()
	{
	
		global $config_data;

		if($config_data['mailer_type']!='swiftmailer' && $config_data['mailer_type']!='custom')
		{
			
			$output=ob_get_contents();
			
			ob_end_clean();

			$check_error_lang[1]='Error: Need install swiftmailer or a custom mail interface with attachments support.';
			$check_error_lang[0]='Error: Need install swiftmailer or a custom mail interface with attachments support.';

			show_error($check_error_lang[0], $check_error_lang[1], $output);

			die;

		}
	
	}

}

?>