<?php

function StatsView()
{

	global $arr_cache_jscript, $arr_cache_header, $arr_cache_local_css, $arr_cache_jscript_module, $lang, $user_data, $base_url;

	$arr_cache_jscript[]='jquery.min.js';
	$arr_cache_jscript['maillist']='jquery-ui.custom.min.js';
	$arr_cache_jscript[]='charts--highcharts--js--highcharts.js';
	$arr_cache_jscript[]='charts--charts.js';
	
	$arr_cache_jscript_module['maillist']='maillist';
	
	$arr_cache_local_css['maillist'][]='jquery-ui.custom.min.css';
	
	ob_start();
	
	?>
	<script language="javascript">
	
	$(document).ready( function () {
	
		url='<?php echo make_fancy_url($base_url, 'maillist/frontend', 'stats', 'stats', array('op' => 1)); ?>';
				
		title='<?php echo $lang['maillist']['email_in_lists']; ?>';
		
		title_y='<?php echo $lang['maillist']['num_emails']; ?>';
		
		date_begin='';
		date_end='';
		datos_add='';
		
		csrf_token='<?php echo $user_data['key_csrf']; ?>';
		
		arr_opciones={type_graph:'column', rotation_x: -45, align_x: 'right'};
		
		$('#stats_form').ShowChartBeauty('show_stats_num_email', url, title, title_y, date_begin, date_end, datos_add, csrf_token, arr_opciones);
		
		$('#modify').click( function () {
		
			$('#change_stats_field_form').change();
		
		});
		
		$('#change_stats_field_form').change( function () {
			
			date_begin=$('#from').val();
			date_end=$('#to').val();
			
			if($(this).val()=='show_stats_num_email_sended')
			{
			
				arr_opciones.type_graph='column';
				
				//arr_opciones={type_graph:'columns', rotation_x: 0, align_x: 'center', num_elements_showed_text: 10};
			
				title='<?php echo $lang['maillist']['num_emails_sended']; ?>';
		
				title_y='<?php echo $lang['maillist']['num_emails']; ?>';
				
				
				$('#stats_form').ShowChartBeauty('show_stats_num_email_sended', url, title, title_y, date_begin, date_end, datos_add, csrf_token, arr_opciones);
				
			}
			if($(this).val()=='num_emails_sended_by_date')
			{
				
				arr_opciones.type_graph='spline';
			
				title='<?php echo $lang['common']['date']; ?>';
		
				title_y='<?php echo $lang['maillist']['num_emails_sended_by_date']; ?>';
				
				$('#stats_form').ShowChartBeauty('num_emails_sended_by_date', url, title, title_y, date_begin, date_end, datos_add, csrf_token, arr_opciones);
			
			}
			else
			if($(this).val()=='show_stats_num_email')
			{
				
				arr_opciones.type_graph='column';
			
				title='<?php echo $lang['maillist']['email_in_lists']; ?>';
		
				title_y='<?php echo $lang['maillist']['num_emails']; ?>';
				
				$('#stats_form').ShowChartBeauty('show_stats_num_email', url, title, title_y, date_begin, date_end, datos_add, csrf_token, arr_opciones);
			
			}
		});
		
		$( "#from" ).datepicker({
			defaultDate: "+1w",
			/*changeMonth: true,*/
			numberOfMonths: 1,
			dateFormat: 'dd-mm-yy',
			maxDate: "0D",
			onSelect: function(dateText, inst) 
			{ 
			
				
			
			},
		});
	
		$( "#to" ).datepicker({
		
			defaultDate: "+1w",
			/*changeMonth: true,*/
			numberOfMonths: 1,
			dateFormat: 'dd-mm-yy',
			maxDate: "0D",
			onSelect: function(dateText, inst) 
			{ 
			
			},
		});
		
	
	});
	
	</script>
	<?php
	
	$arr_cache_header[]=ob_get_contents();
	
	ob_end_clean();
	
	$arr_options=array(0, $lang['maillist']['email_in_lists'], 'show_stats_num_email', $lang['maillist']['email_sended_by_lists'], 'show_stats_num_email_sended', $lang['maillist']['num_emails_sended_by_date'], 'num_emails_sended_by_date');
	
	?>
	<h1><?php echo $lang['maillist']['stats']; ?></h1>
	<div id="stats_form">
	</div>
	<p><strong><?php echo $lang['maillist']['choose_option_stat']; ?>: <?php echo SelectForm('change_stats', '', $arr_options); ?></strong> Desde: <input type="text" id="from" name="from" style="width:100px;" /> Hasta:  <input type="text" id="to" name="to" style="width:100px;" /> <input type="button" value="Actualizar" id="modify"/></p>
	<?php

}

?>