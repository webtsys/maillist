<?php
ob_start();

$_SERVER['HTTP_ACCEPT_LANGUAGE']='en-US';
$_SERVER['REQUEST_URI']='';

include('../framework.php');
ob_end_clean();

load_model('maillist');
load_lang('maillist');
load_libraries(array('send_email'));

global $script_base_controller;

$script_base_controller='maillist';

//include($base_path.'models/maillist.php');
//include($base_path.'libraries/send_email.php');

function order_by_domain($arr_domain)
{

	$arr_order=array();

	foreach($arr_domain as $email)
	{

		$split_domain=explode("@", $email);
		$domain_mail[ $split_domain[1] ][]=$email;

	}

	foreach($domain_mail as $arr_email)
	{

		foreach($arr_email as $email)
		{

			$arr_order[]=$email;

		}

	}

	return $arr_order;

}


//Vemos si el usuario que ha encargado la lista de correo es un admin.

settype($argv[1], 'integer');

settype($argv[2], 'integer');

$idmaillist=$argv[1];

$send_trial=$argv[2];

$query=$model['maillist']->select('where IdMaillist='.$idmaillist);

$arr_maillist=webtsys_fetch_array($query);

settype($arr_maillist['iduser'], 'integer');

$query=$model['user']->select('where IdUser='.$arr_maillist['iduser'], array('privileges_user', 'key_connection'));

list($privileges_user, $key_connection)=webtsys_fetch_row($query);

settype($privileges_user, 'integer');

if($privileges_user==2 && $key_connection==$arr_maillist['token'])
{

	$arr_type[$arr_maillist['type']]='plain';
	$arr_type[0]='plain';
	$arr_type[1]='html';
	$type_mail=$arr_type[$arr_maillist['type']];

	$bcc_mail=array();

	$num_mails=0;
	
	//Here load the list.
	
	$arr_data_list=$model['maillist_name']->select_a_row($arr_maillist['idlist']);
	
	$query=$model['maillist_email']->select('where idlist='.$arr_maillist['idlist'].' and activation=1', array('email'));
	
	while ( list( $email ) = webtsys_fetch_row( $query ) )
	{	
	
		$bcc_mail[]=$email;
		$num_mails++;

	}
	
	if($num_mails==0)
	{
		
		if($arr_data_list['table_related']=='')
		{

			$query=$model['user']->select('where IdUser>0 and yes_list=1', array('email'));

			while ( list( $email ) = webtsys_fetch_row( $query ) )
			{	

				$bcc_mail[]=$email;
				$num_mails++;
				

			}
			
		}
		else
		if($arr_data_list['table_related']!='')
		{
		
			$query=webtsys_query('select '.$arr_data_list['field_related'].' from '.$arr_data_list['table_related']);
			
			while ( list( $email ) = webtsys_fetch_row( $query ) )
			{	

				$bcc_mail[]=$email;
				$num_mails++;
				

			}
			
		}
		
	}
	
	//print_r($bcc_mail);
	//echo count($bcc_mail)."\n";

	//natsort($bcc_mail);
	
	if(count($bcc_mail)>0)
	{
		$bcc_mail=order_by_domain($bcc_mail);
	}

	if($send_trial==1)
	{

		$bcc_mail=array($config_data['portal_email']);
		$num_mails=1;

	}

	//print_r($bcc_mail);

	//Inserto el numero de emails que hay dentro del array en tabla sendmail si está no existe. Si no simplemente saco los datos de ella.

	$num_send=$model['sendmail']->select_count('where idmaillist='.$arr_maillist['IdMaillist'], 'IdMaillist');

	if($num_send==0)
	{

		$arr_sendmail['num_emails']=$num_mails;
		$arr_sendmail['last_email']=0;
		$arr_sendmail['idmaillist']=$arr_maillist['IdMaillist'];

		$query=$model['sendmail']->insert($arr_sendmail);
		
	}
	else
	{

		$query=$model['sendmail']->select('where idmaillist='.$arr_maillist['IdMaillist']);

		$arr_sendmail=webtsys_fetch_array($query);

		//Aqui cortamos el numero de emails.

		$bcc_mail=array_slice($bcc_mail, $num_mails-$arr_sendmail['num_emails']);

	}

	
	//Envio emails de 10 en 100 en el bcc a partir del ultimo email, si no hay ninguno, empiezo dsde el principio. 

	//Divido el numero de emails por 100, lo que me dará igual al numero de lotes que enviaré+1

	$num_loop=ceil(($arr_sendmail['num_emails']/100));

	//echo $num_loop;

	//Hago el bucle y envio.Entre envio y envio habrá 1 segundo de descanso. Así enviaremos 36000 emails en 1 hora pero sin petar el server.

	//$rest_emails=$arr_sendmail['num_emails'];
	//print_r($bcc_mail);

	//Extract first email

	//$email_sending=$bcc_mail[0];
	//next($bcc_mail);
	$email_sending=array_shift($bcc_mail);
	/*echo $email_sending;
	die;*/
	for($x=0;$x<$num_loop;$x++)
	{

		$bcc_final='';
		//$rest_emails=$arr_sendmail['num_emails'];

		if($arr_sendmail['num_emails']>100)
		{

			$rest_emails=100;

		}
		else
		{

			$rest_emails=$arr_sendmail['num_emails'];

		}

		for($c=0;$c<$rest_emails;$c++)
		{

			/*echo $bcc_mail[$c]."\n";
			unset($bcc_mail[$c]);*/
			//$key=key($bcc_mail);
			//echo $bcc_mail[$key]."\n";
//			$bcc_final.=$bcc_mail[$key].' ';
			$bcc_final.=current($bcc_mail).' ';
			next($bcc_mail);
			//echo $c;

		}
		
		$bcc_final=str_replace(' ', ',', trim($bcc_final));
		
		$arr_sendmail['num_emails']-=$rest_emails;

		//Enviamos emails.
		//echo $bcc_final."\r\n";

		$arr_maillist['title']=html_entity_decode( $arr_maillist['title'], ENT_QUOTES, 'UTF-8');
		$arr_maillist['message']=html_entity_decode( unform_text($arr_maillist['message']), ENT_QUOTES, 'UTF-8');
		
		//Obtain text from module if exists...
		
		if($arr_maillist['module']!='')
		{
		
			//Check if exists module...
			
			if(file_exists($base_path.'modules/maillist/modules/'.$arr_maillist['module'].'.php'))
			{
			
				load_libraries(array($arr_maillist['module']), $base_path.'modules/maillist/modules/');
				
				$func_mail=ucfirst($arr_maillist['module']).'Maillist';
				
				if(function_exists($func_mail))
				{
				
					$arr_maillist['message']=$func_mail($arr_maillist['title'], $arr_maillist['message']);
				
				}
			
			}
		
		}
		
		send_mail($email_sending, $arr_maillist['title'], $arr_maillist['message'], $type_mail, $bcc_final);

		//Actualziamos numero de emails.

		$query=$model['sendmail']->update(array('num_emails' => (($arr_sendmail['num_emails'])-1)), 'where idmaillist='.$arr_maillist['IdMaillist']);

		sleep(1);

	}

	//Por cada bucle resto 10 emails a la variable y añado el ultimo email enviado via bcc a la base de datos.

	//Cuando haya terminado, elimino las filas en base de datos que cree. 

	$query=$model['maillist']->delete('where IdMaillist='.$arr_maillist['IdMaillist']);
	$query=$model['sendmail']->delete('where idmaillist='.$arr_maillist['IdMaillist']);

	//echo 'Operación realizada con éxito a las '.date('H:j:s');

	send_mail($config_data['portal_email'], 'Se terminó el envio de emails', 'Operación realizada con éxito a las '.date('H:i:s'), 'plain');

}

return 1;

?>
