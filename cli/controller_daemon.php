<?php

function DaemonCli()
{

	global $model, $config_data, $base_path, $arr_maillist;
	
	//Catch sigkill
	
	declare(ticks = 1);
	
	//define(SIGTERM, 15);
	
	pcntl_signal(SIGTERM, "signal_handler");
	
	load_model('maillist', 'user');
	load_lang('maillist', 'common');
	load_libraries(array('send_email'));

/*	$shortopts = "m:c:f:";  // Valor obligatorio

	$options = getopt($shortopts);
	var_dump($options);*/
	
	$options_final=get_opts_cli('', $arr_opts=array('maillist_id:', 'stop'));
	
	settype($options_final['maillist_id'], 'integer');
	
	if($options_final['maillist_id']==0)
	{
	
		echo json_encode(array('success' => 0, 'txt_error' => 'Need maillist_id parameter'));
		die;
	
	}
	
	$arr_maillist_name=$model['maillist_name']->select_a_row($options_final['maillist_id']);
	
	settype($arr_maillist_name['IdMaillist_name'], 'integer');
	
	if($arr_maillist_name['IdMaillist_name']==0)
	{
	
		echo json_encode(array('success' => 0, 'txt_error' => 'Need maillist_id parameter'));
		die;
	
	}
	
	//Obtain user from list
	
	$arr_user=$model['user']->select_a_row($arr_maillist_name['iduser'], array(), true);
	
	$c_mail=$model['maillist_email']->select_count('where idlist='.$arr_maillist_name['IdMaillist_name'].' and email!=""', 'IdMaillist_email');
	
	//Check if the maillist is sending.
	
	//Obtain email data from maillist table
	
	$query=$model['maillist']->select('where idlist='.$arr_maillist_name['IdMaillist_name'].' and finished=0');
	
	$arr_maillist=webtsys_fetch_array($query);
	
	settype($arr_maillist['IdMaillist'], 'integer');
	
	if($arr_maillist['IdMaillist']==0)
	{
	
		echo json_encode(array('success' => 0, 'txt_error' => 'Need a maillist row in db'));
		die;
	
	}
	
	$arr_maillist['title']=$arr_maillist['title'];
	$arr_maillist['message']=$arr_maillist['message'];
	
	$arr_type[$arr_maillist['type']]='plain';
	$arr_type[0]='plain';
	$arr_type[1]='html';
	$type_mail=$arr_type[$arr_maillist['type']];
	
	//Attached files
		
	$arr_attachments=SerializeField::unserialize($arr_maillist['attachment']);

	if($arr_attachments==false)
	{		

		$arr_attachments=array();

	}
	
	//Insert row of new list in db.
	
	$pid=posix_getpid();
	
	$domain_mail=array();
	$domain_mail_id=array();
	$arr_domain_count=array();
	
	if($arr_maillist['trial']==0)
	{
	
		$model['sendmail']->insert(array('num_emails' => $c_mail, 'pid' => $pid, 'num_email_sended' => 0, 'idmaillist' => $arr_maillist['IdMaillist'], 'idmaillist_name' => $arr_maillist_name['IdMaillist_name'], 'finished' => 0));
		
		$arr_emails=$model['maillist_email']->select_to_array('where idlist='.$arr_maillist['idlist'].' and email!="" and activation=1');
		
		//order emails by domain
		
		foreach($arr_emails as $arr_email)
		{

			$split_domain=explode("@", $arr_email['email']);
			$domain_mail[ $split_domain[1] ][]=$arr_email['email'];
			$domain_mail_id[$arr_email['email']]=$arr_email['IdMaillist_email'];
			
			settype($arr_domain_count[$split_domain[1]], 'integer');
			$arr_domain_count[$split_domain[1]]++;

		}
		
	}
	else
	{
	
		$split_domain=explode("@", $arr_user['email']);
		$domain_mail[ $split_domain[1] ][]=$arr_user['email'];
		$domain_mail_id[$arr_user['email']]=0;
		$arr_domain_count[$split_domain[1]]=1;
	
	}
	
	arsort($arr_domain_count);
	
	foreach($arr_domain_count as $domain => $num_emails_domain)
	{
		
		$arr_domains[]=$domain;
	
	}
	
	$z=0;
	
	$c_sleep=0;
	
	$c_domain=count($domain_mail);
	
	$c=count($domain_mail, COUNT_RECURSIVE)-count($domain_mail);
	
	//$arr_domains=array_keys($domain_mail);
	
	$total_emails_send=0;
	
	$funcmail='no_process_email';
	
	if(file_exists($base_path.'modules/maillist/modules/'.$arr_maillist['module'].'.php'))
	{
	
		load_libraries(array($arr_maillist['module']), $base_path.'modules/maillist/modules/');
		
		$func_mail=ucfirst($arr_maillist['module']).'Maillist';
		
	}
	
	$arr_email_sending=array();
	$arr_email_error=array();
	$total_emails_error=0;
	
	while($total_emails_send!=$c)
	{
	
		foreach($arr_domains as $domain_key)
		{
		
			if($arr_domain_count[$domain_key]>0)
			{
			
				list($idmail, $email_sending)=each($domain_mail[$domain_key]);
			
				$final_message=$arr_maillist['message'];
			
				if($arr_maillist['module']!='')
				{
				
					//Check if exists module...
					
					$final_message=$func_mail($arr_maillist['title'], $final_message, array('maillist_name' => $arr_maillist_name, 'mail' => $arr_maillist, 'idmail' => $domain_mail_id[$email_sending]));
					
					
				
				}
				
				if(!send_mail($email_sending, $arr_maillist['title'], $final_message, $type_mail, '', $arr_attachments))
				{
				
					//echo json_encode(array('success' => 0, 'txt_error' => 'Error sending email '.$email_sending.' N '.($x+1)));
					//die;
					$arr_email_error[]=$email_sending;
					$total_emails_error++;
					
					$model['maillist_email']->reset_require();
					
					$model['maillist_email']->update(array('activation' => 0, 'because_disabled' => 'Email rejected, check your email for see the reason'), 'where email="'.$email_sending.'"');
				
					$model['maillist_email']->reload_require();
				}
				
				$total_emails_send++;
			
				$c_sleep++;
				
				if($c_sleep==3)
				{
				
					sleep(1);
				
					$c_sleep=0;
				
				}
				
				$arr_email_sending[]=$email_sending;
				
				$arr_domain_count[$domain_key]--;
			
			}
			else
			{
			
				unset($domain_mail[$domain_key]);
			
			}
		
		}
		
	}
	
	/*$arr_email_sending=array();
	
	for($x=0;$x<$c;$x++)
	{
	
		//echo current($domain_mail[$arr_domains[$z]])."\n";
		
		$final_message=$arr_maillist['message'];
		
		if(list($idmail, $email_sending)=each($domain_mail[$arr_domains[$z]]))
		{
			//Process email.
			
			if($arr_maillist['module']!='')
			{
			
				//Check if exists module...
				
				$final_message=$func_mail($arr_maillist['title'], $final_message, array('maillist_name' => $arr_maillist_name, 'mail' => $arr_maillist, 'idmail' => $domain_mail_id[$email_sending]));
				
				
			
			}
			
			
			if(!send_mail($email_sending, $arr_maillist['title'], $final_message, $type_mail, '', $arr_attachments))
			{
			
				echo json_encode(array('success' => 0, 'txt_error' => 'Error sending email '.$email_sending.' N '.($x+1)));
				die;
			
			}
			
			$arr_email_sending[]=$email_sending;
			
			//$query=$model['sendmail']->update(array('num_email_sended' => $total_emails_send), 'where idmaillist='.$arr_maillist['IdMaillist']);

			//$query=$model['sendmail']->update(array('num_emails' => (($arr_sendmail['num_emails'])-1)), 'where idmaillist='.$arr_maillist['idlist']);
			
			//unset($domain_mail[$arr_domains[$z]]);
			++$total_emails_send;
			
			$c_sleep++;
			
			if($c_sleep==3)
			{
			
				sleep(1);
				
				//'num_email_sended' => $total_emails_send)
				
				//$query=$model['sendmail']->update(array('num_email_sended' => $total_emails_send), 'where idmaillist='.$arr_maillist['IdMaillist']);
			
				$c_sleep=0;
			
			}
			
		}
		
		
	
	}*/
	
	//print_r($domain_mail);
	
	//echo $options_final['maillist_id'];
	
	$model['sendmail']->reset_require();
	$model['maillist']->reset_require();
	
	if(!$model['maillist']->update(array('finished' => 1), 'where IdMaillist='.$arr_maillist['IdMaillist']))
	{
	
		echo $model['maillist']->std_error."\n";
	
	}
	if(!$model['sendmail']->update(array('finished' => 1, 'num_email_sended' => $total_emails_send, 'date' => TODAY_FIRST), 'where idmaillist='.$arr_maillist['IdMaillist']))
	{
	
		echo $model['sendmail']->std_error."\n";
	
	}

	//echo 'Operación realizada con éxito a las '.date('H:j:s');

	send_mail($arr_user['email'], 'Se terminó el envio de emails', 'Operación terminada con éxito a las '.date('H:i:s')."\n\nSe enviaron en total ".$total_emails_send." emails, de estos envíos, ".$total_emails_error." fueron erroneos y se desactivaron, guardando la razón de que no se pudieran enviar en la base de datos para su posterior arreglo", 'plain');
	
	echo json_encode(array('success' => 1, 'txt_error' => '', 'message' => $arr_maillist['message'], 'email_sended_to' => $arr_email_sending, 'email_error' => $arr_email_error));
	die;
	
}


function signal_handler($signal) 
{

	global $arr_maillist;
	
	$model['sendmail']->reset_require();

	$query=$model['sendmail']->update(array('num_email_sended' => $total_emails_send), 'where idmaillist='.$arr_maillist['IdMaillist']);

}

function no_process_email($title, $final_message)
{

	return $final_message;

}

?>