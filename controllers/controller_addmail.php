<?php

function Addmail()
{

	global $user_data, $model, $ip, $lang, $config_data, $base_path, $base_url, $cookie_path, $arr_block, $prefix_key, $block_title, $block_content, $block_urls, $block_type, $block_id, $config_data;

	ob_start();
	
	load_model('maillist');
	load_lang('maillist');

	$cont_index_page='';

	$arr_block='';

	$arr_block=select_view(array('maillist'));
	
	settype($_POST['idlist'], 'integer');
	
	$query=$model['maillist_name']->select('where IdMaillist_name='.$_POST['idlist'], array('IdMaillist_name', 'name'));
	
	list($idlist, $name)=webtsys_fetch_row($query);
	
	settype($idlist, 'integer');
	
	$email=@$model['maillist_email']->components['email']->check($_POST['email']);
	
	$error=0;
	
	if($email!='')
	{
	
		if($idlist>0)
		{
		
			$post=array('email' => $email, 'idlist' => $idlist, 'date' => TODAY);
							
			if( !($model['maillist_email']->insert($post)) )
			{
				echo load_view(array($lang['maillist']['error_adding_email'], $lang['maillist']['email_exists']), 'content');
				
				$error++;
			
			}
		
		}
		else
		{
		
			$query=$model['maillist_name']->select('', array('IdMaillist_name'));
		
			while(list($idlist)=webtsys_fetch_row($query))
			{
			
				$post=array('email' => $email, 'idlist' => $idlist, 'date' => TODAY);
							
				if(!($model['maillist_email']->insert($post)))
				{
				
					echo load_view(array($lang['maillist']['error_adding_email'], $lang['maillist']['email_exists']), 'content');
					
					$error++;
					
					break;
				
				}
			
			}
		
		}
		
		if($error==0)
		{
		
			echo load_view(array($lang['maillist']['success_adding_email'], $lang['maillist']['success_adding_email_explain']), 'content');
			
		}
		
	}
	else
	{
	
		echo load_view(array($lang['maillist']['error_adding_email'], $lang['maillist']['error_adding_email_explain']), 'content');
	
	}

	$cont_index_page.=ob_get_contents();

	ob_end_clean();

	echo load_view(array($lang['maillist']['add_email_list'], $cont_index_page, $block_title, $block_content, $block_urls, $block_type, $block_id, $config_data), $arr_block);

}

?>
