<?php

function Index()
{

	global $user_data, $model, $ip, $lang, $config_data, $base_path, $base_url, $cookie_path, $arr_block, $prefix_key, $block_title, $block_content, $block_urls, $block_type, $block_id, $config_data;

	ob_start();
	
	load_model('maillist');
	load_lang('maillist');

	$cont_index_page='';

	$arr_block='';

	$arr_block=select_view(array('maillist'));
	
	//echo '<form action="'.make_fancy_url($base_url, 'maillist', 'addmail', 'addmail', array()).'" method="post">';

	set_csrf_key();

	//echo '<p><label>'.$lang['common']['email'].'<br />'.TextForm('email', '', '').'</p>';
	
	$arr_forms=array();
	
	//($name_form, $name_field, $form, $label, $type, $required=0, $parameters='')
	
	$arr_forms['email']=new ModelForm('addmail', 'email', 'TextForm', $lang['common']['email'], new CharField(255), 1, '');

	$query=$model['maillist_name']->select('', array('IdMaillist_name', 'name'));

	$arr_list=array(0, $lang['maillist']['all_lists'], 0);

	while(list($idlist, $name)=webtsys_fetch_row($query))
	{

		$arr_list[]=$name;
		$arr_list[]=$idlist;

	}
	
	$arr_forms['idlist']=new ModelForm('addmail', 'idlist', 'SelectForm', $lang['maillist']['list'], new IntegerField(), 1, $arr_list);

	//echo '<p align="center">'.$lang['common']['list'].'<br />'.SelectForm('idlist', '', $arr_list).'</p>';

	//echo '<p align="center"><input type="submit" value="'.$lang['common']['send'].'"/></p>';

	//echo '</form>';
	
	//UpdateModelFormView($model_form, $arr_fields=array(), $url_post, $enctype='')
	
	echo load_view(array($arr_forms, array(), make_fancy_url($base_url, 'maillist', 'addmail', 'addmail', array()) ), 'common/forms/updatemodelform');
	
	$form=ob_get_contents();
	
	ob_clean();
	
	echo load_view(array($lang['maillist']['add_email_list'], $form), 'content');

	$cont_index_page.=ob_get_contents();

	ob_end_clean();

	echo load_view(array($lang['maillist']['add_email_list'], $cont_index_page, $block_title, $block_content, $block_urls, $block_type, $block_id, $config_data), $arr_block);

}

?>
