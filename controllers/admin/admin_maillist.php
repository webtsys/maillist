<?php

function MaillistAdmin()
{

	global $lang, $base_url, $model, $base_path, $user_data;

	load_model('maillist');
	load_lang('maillist');
	load_libraries(array('forms/textareabb', 'generate_admin_ng', 'admin/generate_admin_class'));
	load_libraries(array('sendmail_class'), $base_path.'modules/maillist/libraries/');

	//Create maillist class.
	
	$maillist=new SendMaillist();
			
	$maillist->url_back=set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule']));

	$maillist->url_post=set_admin_link( 'maillist', array('op' => 3, 'IdModule' => $_GET['IdModule']));
	
	ob_start();
	
	settype($_GET['op'], 'integer');

	$post=array();

	switch($_GET['op'])
	{
	
		default:
		
			/*$cont_admin=ob_get_contents();
	
			ob_end_clean();*/
			
			//<a href="'.set_admin_link( 'maillist', array('op' => 2, 'IdModule' => $_GET['IdModule'], 'idlist' => 0)).'">'.$lang['maillist']['send_mail_list'].' - '.$lang['common']['registered_users'].'</a> | 
			
			echo '<p><a href="'.set_admin_link( 'maillist', array('op' => 8, 'IdModule' => $_GET['IdModule'], 'idlist' => 0)).'">'.$lang['maillist']['assign_users'].'</a> | <a href="'.set_admin_link( 'maillist', array('op' => 9, 'IdModule' => $_GET['IdModule'])).'">Configuración</a>';
			
			//Now, go to other list...
			
			$arr_fields=array('name', 'table_related');
			$arr_fields_edit=array('name', 'table_related', 'field_related', 'module_default');
			$url_options=set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule']));
			
			$model['maillist_name']->label=$lang['maillist']['maillist'];
			
			$model['maillist_name']->create_form();
			
			$model['maillist_name']->forms['name']->label=$lang['common']['name'];
			
			$model['maillist_name']->forms['table_related']->label=$lang['maillist']['table_related']; 
			
			$model['maillist_name']->forms['field_related']->label=$lang['maillist']['field_related'];
			
			$model['maillist_name']->forms['module_default']->label=$lang['maillist']['module_default'];
			
			list($arr_modules_mail, $arr_def_modules_mail)=SendMaillist::get_modules_array();
			
			$model['maillist_name']->forms['module_default']->SetParameters($arr_modules_mail);
			
			$model['maillist_name']->components['module_default']->arr_values=$arr_def_modules_mail;
			
			ob_end_flush();
			
			echo '<h3>'.$lang['maillist']['edit_mail_list_ext'].'</h3>';
			
			generate_admin_model_ng('maillist_name', $arr_fields, $arr_fields_edit, $url_options, $options_func='MailOptionsListModel', $where_sql='');
			
			
			
			//echo load_view(array($lang['maillist_admin']['maillist_admin_name'], $cont_admin), 'content');
		
		break;
		
		case 1:
		
			ob_end_flush();
			
			settype($_GET['idlist'], 'integer');
			
			$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array('IdMaillist_name', 'name'));
			
			list($idlist, $name_list)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
			
			if($idlist>0)
			{
			
				$arr_fields=array('email');
				$arr_fields_edit=array();
				
				$url_options=set_admin_link( 'maillist', array('op' =>1, 'IdModule' => $_GET['IdModule'], 'idlist' => $_GET['idlist']) );
				
				echo '<h3>'.$lang['maillist']['edit_mail_from_maillist'].' - '.$name_list.'</h3>';
				
				$model['maillist_email']->label=$lang['maillist']['email'];
				
				$model['maillist_email']->create_form();
				
				$model['maillist_email']->forms['email']->label=$lang['common']['email'];
				$model['maillist_email']->forms['idlist']->form='HiddenForm';
				$model['maillist_email']->forms['idlist']->SetForm($_GET['idlist']);
				
				generate_admin_model_ng('maillist_email', $arr_fields, $arr_fields_edit, $url_options, $options_func='BasicOptionsListModel', $where_sql='where idlist='.$idlist);
				
				
				
				echo '<p><a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['maillist']['go_back_home_maillist'].'</a></p>';
			}
		
		break;

		case 2:
		
			
			
			$maillist->show_form();
		
			/*settype($_GET['idlist'], 'integer');
		
			$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array());
			
			list($idlist, $name_list, $table_related, $field_related, $module_default)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
			
			$arr_name_list=array();
			$arr_name_list[$idlist]=$name_list;
			$arr_name_list[0]=$lang['common']['registered_users'];
			
			?>
			<form method="post" action="<?php echo set_admin_link( 'maillist', array('op' => 3, 'IdModule' => $_GET['IdModule'])); ?>">
			<?php echo set_csrf_key(); ?>
			<div class="form">
				<p><label for="type"><?php echo $lang['maillist']['send_owner_mail']; ?>:</label><select name="send_trial"><option value="0"><?php echo $lang['common']['no']; ?></option><option value="1"><?php echo $lang['common']['yes']; ?></option></select></p>
				<p><label for="type"><?php echo $lang['common']['title']; ?>:</label><input type="text" name="title" /></p>
				<p><label for="type"><?php echo $lang['common']['text']; ?>:</label><?php echo TextAreaBBForm('message', '', ''); ?></p>
				<!--<p>
				<label for="type"><?php echo $lang['maillist']['mail_type']; ?>:</label><select name="type">
					<option value="0"><?php echo $lang['common']['text']; ?></option>
					<option value="1"><?php echo $lang['common']['html']; ?></option>
				</select></p>-->
				<p>
				<label for="module_mail"><?php echo $lang['maillist']['mail_module']; ?>:</label>
				<?php
				
					$arr_modules_mail[0]=$module_default;
				
					echo SelectForm('module_mail', '', $arr_modules_mail);
				
				?>
				</p>
				<p><input type="submit" value="<?php echo $lang['maillist']['send_mail']; ?>" /></p>
				<input type="hidden" name="idlist" value="<?php echo $idlist; ?>" />
			</div>
			</form>
			<?php
			
			$cont_admin=ob_get_contents();
	
			ob_end_clean();
			
			echo load_view(array($lang['maillist']['send_mail_list'].' - '.$arr_name_list[$idlist], $cont_admin), 'content');
			
			echo '<a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['common']['go_back'].'</a>';*/

		break;

		case 3:
			
			$maillist->send_mail();

			/*$errno=0;
			$errstr='';

			settype($_POST['html'], 'integer');
			settype($_POST['send_trial'], 'integer');
			settype($_POST['idlist'], 'integer');
			settype($_POST['module_mail'], 'integer');

			$post['title']=$_POST['title'];
			$post['message']=$_POST['message'];
			$post['type']=1;//$_POST['type'];
			$post['module']=$_POST['module_mail'];
			$post['iduser']=$user_data['IdUser'];
			$post['token']=$user_data['key_connection'];
			$post['idlist']=$_POST['idlist'];
			
			$query=$model['maillist_name']->select('where IdMaillist_name='.$post['idlist'], array());
			
			list($idlist, $name_list, $table_related, $field_related, $module_default)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
			
			$arr_name_list[$idlist]=$name_list;
			$arr_name_list[0]=$lang['common']['registered_users'];
			
			//print_r($post);
			
			$num_list=$model['maillist']->select_count('', 'IdMaillist');
			
			$idmaillist=0;

			$yes_mail=0;
			
			if($num_list==0)
			{
				if($query=$model['maillist']->insert($post))
				{
					$idmaillist=webtsys_insert_id();
					$yes_mail=1;
				}
				else
				{

					echo '<p>'.$lang['common']['error'].'</p>';

				}

			}
			else
			{

				echo '<p>'.$lang['maillist']['mail_in_queue'].'</p>';

			}

			$pidof=system("pidof /usr/bin/php '.$base_path.'modules/maillist/daemon/daemon.php");
			
			settype($pidof, 'integer');
			
			if($idmaillist>0)
			{
			
				if($pidof==0 && $yes_mail==1)
				{

					echo '<p>'.$lang['maillist']['loading_daemon_mail'].'</p>';

					$retval=0;
					
					system('/usr/bin/php '.$base_path.'modules/maillist/daemon/daemon.php '.$idmaillist.' '.$_POST['send_trial'].' > /tmp/log &', $retval);
					
					//echo '/usr/bin/php '.$base_path.'modules/maillist/daemon/daemon.php '.$idmaillist.' '.$_POST['send_trial'].' > /tmp/log &';
					
					if($retval!=0)
					{
			
						echo '<p>'.$lang['maillist']['error_daemon_no_loading'].': '.$retval.'</p>';
			
					}
					else
					{

						echo '<p>'.$lang['maillist']['sending_mail_by_daemon'].'</p>';

					}

				}
				else if($yes_mail==1)
				{

					echo '<p>'.$lang['maillist']['daemon_sending_list_wait'].'</p>';
			
				}
				
			}
			
			$cont_admin=ob_get_contents();
	
			ob_end_clean();
			
			echo load_view(array($lang['maillist']['send_mail_list'].' - '.$arr_name_list[$idlist], $cont_admin), 'content');

			echo '<a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['common']['go_back'].'</a>';*/

			
		break;
		
		case 4:
		
			$maillist->url_post_add=set_admin_link( 'maillist', array('op' => 4, 'IdModule' => $_GET['IdModule'], 'idlist' => $_GET['idlist']));
		
			$maillist->add_mass_email();
		
		break;
		
		/*case 4:
		
			settype($_GET['idlist'], 'integer');
			
			$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array('IdMaillist_name', 'name'));
			
			list($idlist, $name_list)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
		
			$arr_forms['mass_email']=new ModelForm('mass_form', 'mass_email', 'FileForm', $lang['maillist']['mass_emails'], 'FileField', $required=1, $parameters='');
			
			$url_post=set_admin_link( 'maillist', array('op' => 5, 'IdModule' => $_GET['IdModule'], 'idlist' => $_GET['idlist']));
			
			echo load_view(array($arr_forms, array('mass_email'), $url_post, 'enctype="multipart/form-data"'), 'common/forms/updatemodelform');
		
			$cont_form=ob_get_contents();
		
			ob_end_clean();
			
			echo load_view(array($lang['maillist']['proccess_mass_email'].' - '.$name_list, $cont_form), 'content');
			
			echo '<a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['common']['go_back'].'</a>';
		
		break;
		
		case 5:
		
			ob_end_flush();
			
			settype($_GET['idlist'], 'integer');
			
			$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array('IdMaillist_name', 'name'));
			
			list($idlist, $name_list)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
			
			if($idlist>0)
			{
		
				$emails=trim(file_get_contents($_FILES['mass_email']['tmp_name']));
				
				$arr_emails=explode("\n", $emails);
				
				//print_r($arr_emails);
				
				$good=0;
				$error=0;
				
				//$email_check=new MailField();
				
				
				
				foreach($arr_emails as $key_email => $email)
				{
				
					$post=array('email' => $email, 'idlist' => $idlist);
						
					if($model['maillist_email']->insert($post))
					{
					
						$good++;
					
					}
					else
					{
					
						$error++;
					
					}
				
				}
				
				$cont_final='<p>'.$lang['maillist']['mass_email_processing_success'].'</p>';
				$cont_final.='<p>'.$lang['maillist']['mass_email_accepted'].': '.$good.'</p>';
				$cont_final.='<p>'.$lang['maillist']['mass_email_rejected'].': '.$error.'</p>';
				
				echo load_view(array($lang['maillist']['mass_email_processing'].' - '.$name_list, $cont_final), 'content');
				
			}
			
			echo '<a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['common']['go_back'].'</a>';
		
		break;*/
		
		case 6:
		
			/*settype($_GET['idlist'], 'integer');
			
			$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array('IdMaillist_name', 'name'));
			
			list($idlist, $name_list)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
		
			$arr_forms['mass_email']=new ModelForm('mass_form', 'mass_email', 'FileForm', $lang['maillist']['mass_emails'], 'FileField', $required=1, $parameters='');
			
			$url_post=set_admin_link( 'maillist', array('op' => 7, 'IdModule' => $_GET['IdModule'], 'idlist' => $_GET['idlist']));
			
			echo load_view(array($arr_forms, array('mass_email'), $url_post, 'enctype="multipart/form-data"'), 'common/forms/updatemodelform');
		
			$cont_form=ob_get_contents();
		
			ob_end_clean();
			
			echo load_view(array($lang['maillist']['delete_mass_email'].' - '.$name_list, $cont_form), 'content');
			
			echo '<a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['common']['go_back'].'</a>';*/
			
			$maillist->url_post_del=set_admin_link( 'maillist', array('op' => 6, 'IdModule' => $_GET['IdModule'], 'idlist' => $_GET['idlist']));
		
			$maillist->delete_mass_email();
		
		
		break;
		
		/*case 7:
		
			ob_end_flush();
			
			settype($_GET['idlist'], 'integer');
			
			$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['idlist'], array('IdMaillist_name', 'name'));
			
			list($idlist, $name_list)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
			
			if($idlist>0)
			{
		
				$emails=trim(file_get_contents($_FILES['mass_email']['tmp_name']));
				
				$arr_emails=explode("\n", $emails);
				
				//print_r($arr_emails);
				
				$good=0;
				$error=0;
				
				//$email_check=new MailField();
				
				
				
				foreach($arr_emails as $key_email => $email)
				{
				
					$email_check=$model['maillist_email']->components['email']->check($email);
					
					if($email_check!='')
					{
					
						$query=$model['maillist_email']->delete('where idlist='.$idlist.' and email="'.$email_check.'"');
						
						$good++;
						
					}
					else
					{
					
						$error++;
					
					}
				}
				
				$cont_final='<p>'.$lang['maillist']['mass_email_deleting_success'].'</p>';
				$cont_final.='<p>'.$lang['maillist']['mass_email_accepted'].': '.$good.'</p>';
				$cont_final.='<p>'.$lang['maillist']['mass_email_rejected'].': '.$error.'</p>';
				
				echo load_view(array($lang['maillist']['mass_email_deleting'].' - '.$name_list, $cont_final), 'content');
				
				echo '<a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['common']['go_back'].'</a>';
			
				
			}
			
		
		break;*/
		
		case 8:
		
			ob_end_flush();
			
			load_libraries(array('admin/generate_admin_class'));
		
			$model['user_list']->label=$lang['maillist']['user_list'];
		
			$model['user_list']->create_form();
			
			$model['user_list']->forms['iduser']->label=$lang['common']['user'];
		
			$model['user_list']->forms['iduser']->form='SelectModelForm';
		
			$model['user_list']->forms['iduser']->parameters=array('iduser', '', '', 'user', 'private_nick', 'where IdUser>0');
		
			$admin=new GenerateAdminClass('user_list');
		
			$admin->url_options=set_admin_link('assign_user', array('op' => 8));
		
			$admin->arr_fields=array('iduser');
		
			$admin->show();
			
			echo '<p><a href="'.set_admin_link( 'maillist', array('IdModule' => $_GET['IdModule'])).'">'.$lang['maillist']['go_back_home_maillist'].'</a></p>';
		
		break;
		
		case 9:
		
			ob_end_flush();
			
			echo '<h3>Configuración básica</h3>';
			
			load_libraries(array('admin/generate_admin_class'));
			
			$model['config_maillist']->create_form();
			
			$model['config_maillist']->forms['name_app']->label='Nombre de la aplicación mostrado en el frontend';
			
			$arr_config=$model['config_maillist']->select_a_row_where('', array());
			
			SetValuesForm($arr_config, $model['config_maillist']->forms, $show_error=0);
			
			$admin=new GenerateAdminClass('config_maillist');
		
			$admin->url_options=set_admin_link('config_maillist', array('op' => 9));
			
			$admin->url_back=set_admin_link('maillist', array('op' => 0));
		
			//$admin->arr_fields=array('iduser');
		
			$admin->show_config_mode();
		
		break;

	}
	
}

function MailOptionsListModel($url_options, $model_name, $id, $row)
{

	global $lang, $base_url;

	$arr_links=BasicOptionsListModel($url_options, $model_name, $id);
	
	$arr_links[]='<a href="'.set_admin_link( 'maillist', array('op' => 2, 'IdModule' => $_GET['IdModule'], 'idlist' => $id)).'">'.$lang['maillist']['send_mail_list'].'</a>';
	
	if($row['table_related']=='')
	{
	
		$arr_links[]='<a href="'.set_admin_link( 'maillist', array('op' => 1, 'IdModule' => $_GET['IdModule'], 'idlist' => $id)).'">'.$lang['maillist']['admin_emails'].'</a>';
		
		$arr_links[]='<a href="'.set_admin_link( 'maillist', array('op' => 4, 'IdModule' => $_GET['IdModule'], 'idlist' => $id)).'">'.$lang['maillist']['add_mass_email'].'</a>';
		
		$arr_links[]='<a href="'.set_admin_link( 'maillist', array('op' => 6, 'IdModule' => $_GET['IdModule'], 'idlist' => $id)).'">'.$lang['maillist']['delete_mass_email'].'</a>';
		
	}
	
	return $arr_links;

}

?>