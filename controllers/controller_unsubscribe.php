<?php

function Unsubscribe()
{

	global $user_data, $model, $ip, $lang, $config_data, $base_path, $base_url, $cookie_path, $arr_block, $prefix_key, $block_title, $block_content, $block_urls, $block_type, $block_id, $config_data;

	ob_start();
	
	load_model('maillist');
	load_lang('maillist');
	load_libraries(array('send_email'));

	$cont_index_page='';

	$arr_block='';

	$arr_block=select_view(array('maillist'));
	
	//echo '<form action="'.make_fancy_url($base_url, 'maillist', 'addmail', 'addmail', array()).'" method="post">';

	set_csrf_key();

	//echo '<p><label>'.$lang['common']['email'].'<br />'.TextForm('email', '', '').'</p>';
	
	$arr_forms=array();
	
	//($name_form, $name_field, $form, $label, $type, $required=0, $parameters='')
	
	$arr_forms['email']=new ModelForm('deletemail', 'email', 'TextForm', $lang['common']['email'], new CharField(255), 1, '');

	$query=$model['maillist_name']->select('', array('IdMaillist_name', 'name'));

	$arr_list=array(0, $lang['maillist']['all_lists'], 0);

	while(list($idlist, $name)=webtsys_fetch_row($query))
	{

		$arr_list[]=$name;
		$arr_list[]=$idlist;

	}
	
	$arr_forms['idlist']=new ModelForm('addmail', 'idlist', 'SelectForm', $lang['maillist']['list'], new IntegerField(), 1, $arr_list);

	//echo '<p align="center">'.$lang['common']['list'].'<br />'.SelectForm('idlist', '', $arr_list).'</p>';

	//echo '<p align="center"><input type="submit" value="'.$lang['common']['send'].'"/></p>';

	//echo '</form>';
	
	//UpdateModelFormView($model_form, $arr_fields=array(), $url_post, $enctype='')
	
	settype($_GET['op'], 'integer');
	
	switch($_GET['op'])
	{
	
		default:
	
			echo load_view(array($arr_forms, array(), make_fancy_url($base_url, 'maillist', 'unsubscribe', 'unsubscribe', array('op' => 1)) ), 'common/forms/updatemodelform');
		
		break;
		
		case 1:
		
			settype($_POST['idlist'], 'integer');
			
			$_POST['email']=@form_text($_POST['email']);
			
			$where_sql='where email="'.$_POST['email'].'"';
			
			if($_POST['idlist']>0)
			{
			 
				$where_sql.=' and idlist='.$_POST['idlist'];
			 
			}
			
			$arr_email=$model['maillist_email']->select_a_row_where($where_sql);
			
			settype($arr_email['idmail'], 'integer');
			
			if($arr_email['idmail']==0)
			{
			
				$token=get_token();
			
				$url_unsubscribe=controller_fancy_url('unsubscribe', 'unsubscribe', $arr_data=array('op' => 2, 'token' => $token));
			
				$email_instructions='';
				
				ob_start();
				
				?>
<?php echo $lang['maillist']['welcome_subscription']; ?>

				
<?php echo $lang['maillist']['instructions_explain']; ?>


<?php echo $url_unsubscribe; ?>
				
				<?php
				
				$email_instructions=ob_get_contents();
				
				ob_end_clean();
				
				//Insert on table
				
				$post=array('email' => $arr_email['email'], 'token' => $token, 'date' => TODAY);
				
				if($model['unsubscribe_maillist']->insert($post))
				{
					if(!send_mail($arr_email['email'], $lang['maillist']['unsubscribe_instructions'], $email_instructions))
					{
					
						?>
						<p><?php echo $lang['maillist']['no_send_mail']; ?>: <?php echo $config_data['portal_email']; ?></p>
						<?php
					
					}
					else
					{
					
						?>
						<p><?php echo $lang['maillist']['send_mail_with_instructions']; ?></p>
						<?php
					
					}
					
				}
				
			}
			else
			{
			
				?>
				<p><?php echo $lang['maillist']['no_exists_mail']; ?></p>
				<?php
			
			}
		
		break;
		
		case 2:
		
			$_GET['token']=@form_text($_GET['token']);
		
			$arr_unsubscribe=$model['unsubscribe_maillist']->select_a_row_where('where token="'.$_GET['token'].'" and delete_email=0', array(), 1);
			
			settype($arr_unsubscribe['IdUnsubscribe_maillist'], 'integer');
			
			if($arr_unsubscribe['IdUnsubscribe_maillist']>0)
			{
			
				settype($arr_unsubscribe['idlist'], 'integer');
			
				$where_sql='where email="'.$arr_unsubscribe['email'].'"';
				
				if($arr_unsubscribe['idlist']>0)
				{
				
					$where_sql=' and idlist='.$arr_unsubscribe['idlist'];
				
				}
				
				//Obtain data from maillist_email
				
				$arr_email=$model['maillist_email']->select_a_row_where($where_sql);
				
				
				$model['maillist_email']->reset_require();
				
				$yes_update=$model['maillist_email']->update(array('IdMaillist_email' => $arr_email['IdMaillist_email'],'email' => ''), $where_sql, 1);
				
				//echo $yes_update; die;
				
				if($yes_update)
				{
					/*echo $model['maillist_email']->std_error;
					die;*/
					
					$model['unsubscribe_maillist']->reset_require();
				
					$model['unsubscribe_maillist']->update(array('delete_email' => 1, 'email' => '', 'date' => TODAY), 'where token="'.$_GET['token'].'" and delete_email=0', 1);
					
					?>
					<p><?php echo $lang['maillist']['delete_mail_success']; ?></p>
					<?php
				
				}
				else
				{
					if(DEBUG)
					{
					
						echo '<p>Error:'.$model['maillist_email']->std_error.'</p>';
					
					}
					?>
					<p><?php echo $lang['maillist']['error_deleting_mail']; ?>: <?php echo $config_data['portal_email']; ?></p>
					<?php
				
				}
				
				
			}
			else
			{
			
				?>
				<p><?php echo $lang['maillist']['no_exists_mail']; ?></p>
				<?php
			
			}
			
		
			
		
		break;
		
	}
	
	$form=ob_get_contents();
	
	ob_clean();
	
	echo load_view(array($lang['maillist']['delete_email_list'], $form), 'content');

	$cont_index_page.=ob_get_contents();

	ob_end_clean();

	echo load_view(array($lang['maillist']['delete_email_list'], $cont_index_page, $block_title, $block_content, $block_urls, $block_type, $block_id, $config_data), $arr_block);

}

?>
