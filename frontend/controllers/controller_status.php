<?php

function Status()
{

	global $model, $user_data, $lang, $base_url, $base_path, $config_data;
	
	load_libraries(array('utilities/menu_barr_hierarchy'));
	
	//menu_barr_hierarchy($arr_menu, $name_get, $value_get, $yes_last_link=0);
	
	settype($_GET['op'], 'integer');
	
	/*$original_theme=$config_data['dir_theme'];

	$config_data['dir_theme']=$original_theme.'/admin';*/

	$arr_block='admin_none';
	
	load_model('maillist');
	load_lang('maillist');
	load_libraries(array('admin/generate_admin_class'));
	load_libraries(array('forms/textareabb', 'generate_admin_ng'));
	
	load_libraries(array('sendmail_class'), $base_path.'modules/maillist/libraries/');
	
	/*$maillist=new SendMaillist();
			
	$maillist->url_back=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array());
		
	$maillist->url_post=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 3));*/
	
	$c_user=$model['user_list']->select_count('where iduser='.$user_data['IdUser']);
	
	$arr_config=$model['config_maillist']->select_a_row_where('', array());
	
	$title_maillist=I18nField::show_formatted($arr_config['name_app']);
		
	$content='';
	
	/*$arr_menu[0]=array(0 => $lang['maillist']['maillists'], 1 => make_fancy_url($base_url, 'maillist/frontend', 'index', 'index', array('op' => 0)));
	
	$arr_menu[1]=array(0 => $lang['maillist']['maillists'], 1 => make_fancy_url($base_url, 'maillist/frontend', 'index', 'index', array('op' => 0)));*/
	
	//$arr_menu[0]=array('module' => 'module', 'controller' => 'controller', 'text' => 'text', 'name_op' => , 'params' => array())
	
	$arr_menus[0]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['maillists'], 'name_op' => 'op', 'params' => array('op' => 0));
	
	$arr_menus[1]=array('module' => 'maillist/frontend', 'controller' => 'status', 'text' => $lang['maillist']['status_list'], 'name_op' => 'op', 'params' => array('op' => 0));
	
	//echo menu_barr_hierarchy($arr_menu, 'op', $_GET['op'], $yes_last_link=0);
	
	if($c_user>0)
	{
		
		
		switch($_GET['op'])
		{
		
			default:
				
				echo '<h1>'.$lang['maillist']['status_list'].'</h1>';
				
				echo menu_barr_hierarchy_control($arr_menus);
				
				echo '<h2>'.$lang['maillist']['process_emails'].'</h2>';
				
				$model['maillist']->create_form();
				
				$model['maillist']->forms['idlist']->label=$lang['maillist']['maillist'];
				$model['maillist']->forms['problem']->label=$lang['maillist']['problem'];
				
				$model['maillist_name']->create_form();
				
				$model['maillist_name']->forms['name']->label=$lang['common']['name'];
				$model['maillist_name']->forms['programmed']->label=$lang['maillist']['programmed'];
				$model['maillist_name']->forms['repeat']->label=$lang['maillist']['repeat'];
				
				$model['maillist']->components['idlist']->name_field_to_field='name';
				
				$arr_fields=array('idlist');
				
				$where_sql='where finished=0 and maillist.iduser='.$user_data['IdUser'];
				
				$url_options=controller_fancy_url('status', 'status', $arr_data=array(), $respect_upper=0);
				
				$list_process=new ListModelClass('maillist', $arr_fields, $url_options, $options_func='PidOptionsListModel', $where_sql, $arr_fields_form=array(), $type_list='Basic', $no_search=true, $yes_id=1, $yes_options=1, $extra_fields=array(), $separator_element='<br />', $simple_redirect=0);
				
				$list_process->url_options=controller_fancy_url('status', 'status', $arr_data=array('op' => 1), $respect_upper=0);
				
				$list_process->show();
				
				echo '<h2>'.$lang['maillist']['cron_emails'].'</h2>';
				
				$where_sql='where programmed>0 and maillist_name.iduser='.$user_data['IdUser'];
				
				$arr_fields=array();
				
				$list_programmed=new ListModelClass('maillist_name', $arr_fields, $url_options, $options_func='PidOptionsListModel', $where_sql, $arr_fields_form=array(), $type_list='Basic', $no_search=true, $yes_id=1, $yes_options=0, $extra_fields=array(), $separator_element='<br />', $simple_redirect=0);
				
				$list_programmed->arr_fields=array('name', 'programmed', 'repeat');
				
				$list_programmed->show();
				
				echo '<h2>'.$lang['maillist']['finished_emails'].'</h2>';
				
				$where_sql='where finished=1 and maillist.iduser='.$user_data['IdUser'];
				
				$list_finished=new ListModelClass('maillist', $arr_fields, $url_options, $options_func='MaillistOptionsListModel', $where_sql, $arr_fields_form=array(), $type_list='Basic', $no_search=true, $yes_id=1, $yes_options=1, $extra_fields=array(), $separator_element='<br />', $simple_redirect=0);
				
				$list_finished->arr_fields=array('idlist', 'problem');
				
				$list_finished->yes_options=0;
				
				$list_finished->show();
			
				echo '<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
			
			break;
			
			case 1:
			
				settype($_GET['idprocess'], 'integer');
				
				//$arr_mail=$model['maillist']->select_a_row($_GET['idprocess']);
				
				$arr_proccess=$model['sendmail']->select_a_row_where('where idmaillist='.$_GET['idprocess']);
				
				settype($arr_proccess['idmaillist'], 'integer');
				
				$text_explain=$lang['maillist']['no_killing_process'];
				
				if($arr_proccess['idmaillist']>0)
				{
				
					//Kill the process
					
					define('SIGTERM', 15);

					if(posix_kill ( $arr_proccess['pid'] ,  SIGTERM ))
					{
					
						//echo load_view(array($lang['maillist']['killing_process'], $lang['maillist']['killing_process_explain']), 'content');
						
						$text_explain=$lang['maillist']['killing_process'];
					
					}
					/*else
					{
					
						//echo load_view(array($lang['maillist']['no_killing_process'], $lang['maillist']['no_killing_process_explain']), 'content');
					
					}*/
					
					//Update sendmail
					
					$model['sendmail']->reset_require();
					
					$query=$model['sendmail']->update(array('finished' => 1), 'where idmaillist='.$arr_proccess['idmaillist']);
				
				}
				
				$model['maillist']->reset_require();
				
				$query=$model['maillist']->update(array('finished' => 1, 'problem' => 1), 'where IdMaillist='.$_GET['idprocess']);
				
				echo load_view(array($text_explain, $lang['maillist']['killing_process_explain']), 'content');
				
				echo '<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
			
			break;
		
		}
		
	}
	else
	{
		/*$title_maillist=$lang['maillist']['without_permissions'];
	
		echo '<p>'.$lang['maillist']['without_permissions'].'</p>';*/
		
		$url_maillist=make_fancy_url($base_url, 'maillist/frontend', 'index', 'frontend', array());
		
		die(header('Location: '.make_fancy_url($base_url, 'user', 'index', 'login', array('register_page' => urlencode_redirect($url_maillist)), true ) ));
	
	}
	
	$content=ob_get_contents();
	
	ob_end_clean();

	echo load_view(array($title_maillist, $content), 'maillist/mailfrontend');
}

function PidOptionsListModel($url_options, $model_name, $id, $arr_row)
{

	global $model;

	?>
	<script language="javascript">
		function warning()
		{
			if(confirm('¿Quiere parar el envío de la lista de correo?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	</script>
	<?php
	
	$arr_link[]='<a href="'.add_extra_fancy_url($url_options, array('idprocess' => $id)).'" onclick="javascript: if(warning()==false) { return false; }" id="delete_list" title="Cancelar"><span>Cancelar envío</span></a>';
	
	return $arr_link;

}

function MaillistOptionsListModel($url_options, $model_name, $id, $arr_row)
{

	global $model, $base_url;
	
	$url=make_fancy_url($base_url, 'maillist/frontend', 'stats', 'stats', array('idmaillist' => $id));
	
	$arr_link[]='<a href="'.$url.'"><span>Ver estadísticas de este envío</span></a>';
	
	return $arr_link;

}


?>