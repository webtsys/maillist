<?php

function Stats()
{

	global $model, $user_data, $lang, $base_url, $base_path, $config_data;
	
	load_libraries(array('utilities/menu_barr_hierarchy'));
	
	//menu_barr_hierarchy($arr_menu, $name_get, $value_get, $yes_last_link=0);
	
	settype($_GET['op'], 'integer');
	
	/*$original_theme=$config_data['dir_theme'];

	$config_data['dir_theme']=$original_theme.'/admin';*/

	$arr_block='admin_none';
	
	load_model('maillist');
	load_lang('maillist');
	load_libraries(array('admin/generate_admin_class'));
	load_libraries(array('forms/textareabb', 'generate_admin_ng'));
	
	load_libraries(array('sendmail_class'), $base_path.'modules/maillist/libraries/');
	
	/*$maillist=new SendMaillist();
			
	$maillist->url_back=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array());
		
	$maillist->url_post=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 3));*/
	
	$c_user=$model['user_list']->select_count('where iduser='.$user_data['IdUser']);
	
	$arr_config=$model['config_maillist']->select_a_row_where('', array());
	
	$title_maillist=I18nField::show_formatted($arr_config['name_app']);
		
	$content='';
	
	/*$arr_menu[0]=array(0 => $lang['maillist']['maillists'], 1 => make_fancy_url($base_url, 'maillist/frontend', 'index', 'index', array('op' => 0)));
	
	$arr_menu[1]=array(0 => $lang['maillist']['maillists'], 1 => make_fancy_url($base_url, 'maillist/frontend', 'index', 'index', array('op' => 0)));*/
	
	//$arr_menu[0]=array('module' => 'module', 'controller' => 'controller', 'text' => 'text', 'name_op' => , 'params' => array())
	
	$arr_menus[0]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['maillists'], 'name_op' => 'op', 'params' => array('op' => 0));
	
	$arr_menus[1]=array('module' => 'maillist/frontend', 'controller' => 'stats', 'text' => $lang['maillist']['stats'], 'name_op' => 'op', 'params' => array('op' => 0));
	
	//echo menu_barr_hierarchy($arr_menu, 'op', $_GET['op'], $yes_last_link=0);
	
	$jsondata=array();
	
	if($c_user>0)
	{
		
		
		switch($_GET['op'])
		{
		
			default:
				
				echo '<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
				
				echo load_view(array(), 'maillist/stats');
				
				echo '<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
			
			break;
			
			case 1:
			
				settype($_POST['tipo'], 'string');
				
				switch($_POST['tipo'])
				{
				
					case 'show_stats_num_email':
						
						$jsondata['title'][0]=$lang['maillist']['num_emails'];
						
						list($first_date, $last_date)=set_dates(0);
						
						$where_and_sql='';
						
						if($first_date!='')
						{
							
							$where_and_sql=' and date>='.$first_date.' and date<='.$last_date;
						
						}
						
						$arr_maillist=$model['maillist_name']->select_to_array('', array('IdMaillist_name', 'name'));
						
						foreach($arr_maillist as $idmaillist => $arr_mail)
						{
						
							$num_emails=$model['maillist_email']->select_count('where idlist='.$arr_mail['IdMaillist_name'].$where_and_sql);
						
							$jsondata['desc_items'][0][]=$num_emails;
							
							$jsondata['categories'][]=$arr_mail['name'];
							
						}
						
						echo json_encode($jsondata);
					
					break;
					
					case 'show_stats_num_email_sended':
					
					
						$jsondata=array();
						
						list($first_date, $last_date)=set_dates(0);
						
						$where_and_sql='';
						
						if($first_date!='')
						{
							
							$where_and_sql=' and date>='.$first_date.' and date<='.$last_date;
						
						}
						
						settype($_GET['idmaillist_name'], 'integer');
					
						$jsondata['title'][0]=$lang['maillist']['num_emails_sended'];
						
						$where_sql='';
						
						if($_GET['idmaillist_name']>0)
						{
						
							$where_sql='where IdMaillist_name='.$_GET['idmaillist_name'];
						
						}
						
						$arr_maillist=$model['maillist_name']->select_to_array('', array('IdMaillist_name', 'name'));
						
						$arr_keys_mail=array_keys($arr_maillist);
							
						
						
						//print_r($arr_keys_mail);
						
						foreach($arr_maillist as $idmaillist => $arr_mail)
						{
							
							$query=webtsys_query('select SUM(num_email_sended) from sendmail where idmaillist_name='.$arr_mail['IdMaillist_name'].$where_and_sql);
							
							list($num_emails)=webtsys_fetch_row($query);
						
							settype($num_emails, 'integer');
						
							$jsondata['desc_items'][0][]=$num_emails;
							
							$jsondata['categories'][]=$arr_mail['name'];
							
						}
						
						echo json_encode($jsondata);
					
					break;
					
					case 'num_emails_sended_by_date':
					
						list($first_date, $last_date)=set_dates();
						
						$arr_num_email=array();
					
						$jsondata['title'][0]=$lang['maillist']['num_emails_sended_by_date'];
						
						$seconds_day=3600*24;
						
						$arr_date=$model['sendmail']->select_to_array('where date>='.$first_date.' and date<='.$last_date, array('num_email_sended', 'date'));
						
						foreach($arr_date as $send_id => $arr_send)
						{
						
							settype($arr_num_email[$arr_send['date']], 'integer');
						
							$arr_num_email[$arr_send['date']]+=$arr_send['num_email_sended'];
						
						}
						//echo 'where num_email_sended>='.$first_date.' and num_email_sended<='.$last_date;
						for($first_date;$first_date<=$last_date;$first_date+=$seconds_day)
						{
							
							settype($arr_num_email[$first_date], 'integer');
							
							$jsondata['desc_items'][0][]=$arr_num_email[$first_date];
						
							$jsondata['categories'][]=DateField::format_date($first_date);
						
						}
						
						echo json_encode($jsondata);
					
					break;
				
				}
			
				die;
			
			break;
		
		}
		
	}
	else
	{
		/*$title_maillist=$lang['maillist']['without_permissions'];
	
		echo '<p>'.$lang['maillist']['without_permissions'].'</p>';*/
		
		$url_maillist=make_fancy_url($base_url, 'maillist/frontend', 'index', 'frontend', array());
		
		die(header('Location: '.make_fancy_url($base_url, 'user', 'index', 'login', array('register_page' => urlencode_redirect($url_maillist)), true ) ));
	
	}
	
	$content=ob_get_contents();
	
	ob_end_clean();

	echo load_view(array($title_maillist, $content), 'maillist/mailfrontend');
}

function PidOptionsListModel($url_options, $model_name, $id, $arr_row)
{

	global $model;

	?>
	<script language="javascript">
		function warning()
		{
			if(confirm('¿Quiere parar el envío de la lista de correo?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	</script>
	<?php
	
	$arr_link[]='<a href="'.add_extra_fancy_url($url_options, array('idprocess' => $id)).'" onclick="javascript: if(warning()==false) { return false; }" id="delete_list" title="Cancelar"><span>Cancelar envío</span></a>';
	
	return $arr_link;

}

function MaillistOptionsListModel($url_options, $model_name, $id, $arr_row)
{

	global $model;
	
	$arr_link[]='<a href="'.$url_options.'"><span>Ver estadísticas de este envío</span></a>';
	
	return $arr_link;

}

function set_dates($yes_default=1)
{

load_libraries(array('fields/datetimefield'));
					
	settype($_POST['date_end'], 'string');
	settype($_POST['date_begin'], 'string');
	
	$first_date='';
	$last_date='';
	
	if($yes_default==1)
	{
		$first_date=TODAY_FIRST-604800;
	
		$last_date=TODAY_FIRST;
	
	}
	
	$arr_begin=explode('-', $_POST['date_begin']);
	$arr_end=explode('-', $_POST['date_end']);
	
	$date_begin='';
	$date_end='';
	
	if(isset($arr_begin[2]))
	{
		$date_begin=$arr_begin[2].$arr_begin[1].$arr_begin[0].'000000';
	
		$date_begin=DateTimeField::obtain_timestamp_datefield($date_begin);
	
		
	}
	
	if(isset($arr_end[2]))
	{
	
		$date_end=$arr_end[2].$arr_end[1].$arr_end[0].'000000';
	
		
		$date_end=DateTimeField::obtain_timestamp_datefield($date_end);
	

	}
	
	if($date_begin!='' && $date_end!='')
	{
	
		if($date_begin<$date_end)
		{
			$first_date=$date_begin;
		
			$last_date=$date_end;
		}
		
	}
	
	return array($first_date, $last_date);

}

?>