<?php

function Index()
{

	global $model, $user_data, $lang, $base_url, $base_path, $config_data;
	
	settype($_GET['op'], 'integer');
	
	/*$original_theme=$config_data['dir_theme'];

	$config_data['dir_theme']=$original_theme.'/admin';*/

	$arr_block='admin_none';
	
	load_model('maillist');
	load_lang('maillist');
	load_libraries(array('admin/generate_admin_class'));
	load_libraries(array('forms/textareabb', 'generate_admin_ng'));
	
	load_libraries(array('sendmail_class'), $base_path.'modules/maillist/libraries/');
	
	$maillist=new SendMaillist();
			
	$maillist->url_back=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array());
		
	$maillist->url_post=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 3));
	
	$c_user=$model['user_list']->select_count('where iduser='.$user_data['IdUser']);
	
	$arr_config=$model['config_maillist']->select_a_row_where('', array());
	
	$title_maillist=I18nField::show_formatted($arr_config['name_app']);
		
	$content='';
	
	if($c_user>0)
	{
		
		ob_start();
		
		switch($_GET['op'])
		{
		
			default:
			
				echo '<h1>Mis listas de correo</h1>';
				
				list($arr_modules_mail, $arr_def_modules_mail)=SendMaillist::get_modules_array();
				
				$model['maillist_name']->create_form();
				
				$model['maillist_name']->forms['name']->label=$lang['maillist']['maillist'];
			
				/*$model['maillist_name']->forms['table_related']->label=$lang['maillist']['table_related']; 
				
				$model['maillist_name']->forms['field_related']->label=$lang['maillist']['field_related'];*/
				
				$model['maillist_name']->forms['module_default']->label=$lang['maillist']['module_default'];
				
				$model['maillist_name']->components['module_default']->arr_values=$arr_def_modules_mail;
				
				$model['maillist_name']->forms['module_default']->SetParameters($arr_modules_mail);
				
				$model['maillist_name']->forms['iduser']->form='HiddenForm';
				
				$model['maillist_name']->forms['signature']->form='TextAreaBBForm';
				$model['maillist_name']->forms['signature']->label=$lang['maillist']['signature'];
				
				$model['maillist_name']->forms['programmed']->label=$lang['maillist']['programmed'];
				
				$model['maillist_name']->forms['programmed']->parameters=array('programmed', '', '');
				
				$model['maillist_name']->forms['activated']->label=$lang['maillist']['activated'];
				
				$model['maillist_name']->forms['repeat']->label=$lang['maillist']['repeat'];
				
				$_POST['iduser']=$user_data['IdUser'];
				
				$model['maillist_name']->forms['iduser']->SetForm($user_data['IdUser']);
				
				$admin=new GenerateAdminClass('maillist_name');
				
				$admin->txt_list_new=$lang['maillist']['maillists'];
				
				$admin->txt_add_new_item=$lang['maillist']['add_new_list']; //'<span id="add_element">'.$lang['maillist']['add_new_list'].'</span>';
				
				$admin->no_search=true;
				
				$admin->where_sql='where iduser='.$user_data['IdUser'];
				
				$admin->show_id=0;
				
				$admin->arr_fields=array('name');
				
				$admin->arr_fields_edit=array('name', 'module_default', 'signature', 'iduser', 'programmed', 'activated', 'repeat');
				
				$admin->url_options=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array());
				
				$admin->simple_redirect=1;
				
				$admin->class_add='add_element';
				
				$admin->options_func='MailOptionsList';
				
				$admin->separator_element_opt=' ';
				
				$admin->extra_menu_create='| <a href="'.controller_fancy_url('status', 'status', $arr_data=array(), $respect_upper=0).'" class="status_email">Ver estados de envío de listas de correo</a> | <a href="'.controller_fancy_url('stats', 'stats', $arr_data=array(), $respect_upper=0).'" class="stats_email">Estadísticas de correo</a>';
				
				settype($_GET['IdMaillist_name'], 'integer');
				
				if($_GET['IdMaillist_name']>0)
				{
				
					$arr_name=$model['maillist_name']->select_a_row($_GET['IdMaillist_name'], array('name'));
				
					echo '<h2>'.'Editar lista de correo "'.$arr_name['name'].'"'.'</h2>';
				
					$admin->txt_edit_item='Editar lista de correo "'.$arr_name['name'].'"';
					
				
				}
				
				$admin->show_goback=0;
				
				$admin->show();
				
				if($_GET['op_edit']==0)
				{
				
					echo '<h1>Otras listas de correo</h1>';
					
					$model['middle_user_list']->components['idmaillist_name']->label=$lang['maillist']['maillists'];
					
					$model['middle_user_list']->components['idmaillist_name']->name_field_to_field='name';
					
					$model['middle_user_list']->create_form();
					
					$model['middle_user_list']->forms['idmaillist_name']->label=$lang['maillist']['maillist'];
					
					$admin=new SimpleList('middle_user_list', $arr_fields=array(), $url_options='');
					
					$admin->txt_list_new=$lang['maillist']['maillists'];
					
					$admin->arr_fields=array('idmaillist_name');
					
					$admin->options_func='MailNoOwnerOptionsList';
					
					$admin->separator_element=' ';
					
					$admin->show_id=0;
					
					$admin->no_search=1;
					
					$admin->raw_query=0;
					
					$admin->url_options=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array());
					
					$admin->where_sql='where middle_user_list.iduser='.$user_data['IdUser'];
					
					$admin->simple_redirect=1;
					
					$model['maillist_name']->components['name']=new CharField(255);
					
					$admin->show();
					
					$model['maillist_name']->components['name']=new MailCharField(255);
				
				}
				
			break;
		
			case 1:
			
				//ob_end_flush();
			
			settype($_GET['IdMaillist_name'], 'integer');
			
			//.' and maillist_name.iduser='.$user_data['IdUser']
			
			$query=$model['maillist_name']->select('where IdMaillist_name='.$_GET['IdMaillist_name'], array('IdMaillist_name', 'name', 'iduser'));
			
			list($idlist, $name_list, $iduser_owner)=webtsys_fetch_row($query);
			
			settype($idlist, 'integer');
			
			if($idlist>0)
			{
								
				echo '<h3>'.$lang['maillist']['edit_mail_from_maillist'].' '.$name_list.'</h3>';
				
				if($iduser_owner==$user_data['IdUser'])
				{
				
					echo '<p><a href="'.make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 8, 'idlist' => $idlist)).'">Añadir nuevo usuario a esta lista</a></p>';
				
				}
				
				$model['maillist_email']->label=$lang['maillist']['email'];
				
				$model['maillist_email']->create_form();
				
				$model['maillist_email']->forms['email']->label=$lang['common']['email'];
				$model['maillist_email']->forms['idlist']->form='HiddenForm';
				$model['maillist_email']->forms['idlist']->SetForm($idlist);
				$model['maillist_email']->forms['name']->label=$lang['common']['name'];
				$model['maillist_email']->forms['company']->label=$lang['maillist']['company'];
				$model['maillist_email']->forms['date']->label=$lang['maillist']['date_added'];
				$model['maillist_email']->forms['job']->label=$lang['maillist']['job'];
				$model['maillist_email']->forms['activation']->label=$lang['maillist']['activation'];
				
				$admin=new GenerateAdminClass('maillist_email');
				
				$admin->arr_fields=array('email', 'name', 'company', 'activation');
				$admin->arr_fields_edit=array();
				
				$admin->txt_list_new=$lang['maillist']['email_subscription'];
				
				$admin->txt_add_new_item=$lang['maillist']['add_new_list_email']; //'<span id="add_element">'.$lang['maillist']['add_new_list'].'</span>';
				
				$admin->class_add='add_element';
				
				$url_add=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 4, 'idlist' => $_GET['IdMaillist_name']));
				
				$url_delete=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 5, 'idlist' => $_GET['IdMaillist_name']));
				
				$url_csv=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 6, 'idlist' => $_GET['IdMaillist_name']));
				
				$admin->extra_menu_create='| <a href="'.$url_add.'" class="add_mass_email">'.$lang['maillist']['add_mass_email'].'</a> | <a href="'.$url_delete.'" class="delete_mass_email">'.$lang['maillist']['delete_mass_email'].'</a> | <a href="'.$url_csv.'" class="csv_email">'.$lang['maillist']['csv_list'].'</a>';
				
				$admin->url_options=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('IdMaillist_name' => $idlist,'op' => 1));
				
				$admin->simple_redirect=1;
				
				$admin->show_id=0;
				
				$admin->search_asc=$lang['maillist']['alphabetic_order'];
				$admin->search_desc=$lang['maillist']['alphabetic_order_inverse'];
				
				$admin->where_sql='where idlist='.$idlist.' and email!=""';
				
				$admin->no_search=1;
				
				//$admin->options_func='ListOptionsListModel';
				
				$arr_menus[0]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['maillists'], 'name_op' => 'op', 'params' => array('op' => 0));
	
				$arr_menus[1]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['edit_mail_from_maillist'].' '.$name_list, 'name_op' => 'op', 'params' => array('op' => 1 ));
				
				$menu_hierarchy='<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
				
				echo $menu_hierarchy;
				
				$admin->show();
				
				echo $menu_hierarchy;
				
				//generate_admin_model_ng('maillist_email', $arr_fields, $arr_fields_edit, $url_options, $options_func='BasicOptionsListModel', $where_sql='where idlist='.$idlist);
				
				//echo '<p><a href="'.make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' =>0) ).'">'.$lang['maillist']['go_back_home_maillist'].'</a></p>';
			}
			
			break;
			
			case 2:
			
				settype($_GET['idlist'], 'integer');
			
				settype($_GET['iddraft'], 'integer');
			
				$arr_menus[0]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['maillists'], 'name_op' => 'op', 'params' => array('op' => 0));
				
				$arr_menus[1]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => 'Envio puntual', 'name_op' => 'op', 'params' => array('op' => 2, 'idlist' => $_GET['idlist']));
				
				if($_GET['iddraft']>0)
				{
				
					$text_go_back=$lang['maillist']['draft_list'];
				
					$arr_param_back=array('op' => 7 );
				
					$arr_param_back['idlist'] = $_GET['idlist'];
				
					//http://localhost/phangodev/maillist/frontend/show/index/maillist/op/7/idlist/15
				
					$arr_menus[1]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $text_go_back, 'name_op' => 'op', 'params' => $arr_param_back);
					
				}
				
				$menu_hierarchy='<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
			
				echo $menu_hierarchy;
			
				$maillist->show_form();
				
				echo $menu_hierarchy;
			
			break;
			
			case 3:
			
				$maillist->send_mail();
			
			break;
			
			case 4:
			
				//Add emails
				
				settype($_GET['idlist'], 'integer');
				
				$maillist->url_post_add=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 4, 'idlist' => $_GET['idlist']));
		
				$maillist->url_back=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 1, 'IdMaillist_name' => $_GET['idlist']));
		
				$maillist->add_mass_email();
				
			
			break;
			
			case 5:
			
				//Delete emails
			
				settype($_GET['idlist'], 'integer');
				
				$maillist->url_post_del=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 5, 'idlist' => $_GET['idlist']));
		
				$maillist->url_back=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 1, 'IdMaillist_name' => $_GET['idlist']));
		
				$maillist->delete_mass_email();
			
			break;
			
			case 6:
			
				//ob_clean();
			
				settype($_GET['idlist'], 'integer');
				
				$maillist->export_csv($_GET['idlist']);
				
				$cont=ob_get_contents();
				
				ob_end_clean();
				
				echo trim($cont);
				
				die;
			
			break;
			
			case 7:
			
				$arr_menus[0]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['maillists'], 'name_op' => 'op', 'params' => array('op' => 0));
	
				$arr_menus[1]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['draft_list'], 'name_op' => 'op', 'params' => array('op' => 7 ));
				
				$menu_hierarchy='<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
			
				$_POST['iduser']=$user_data['IdUser'];
				
				settype($_GET['idlist'], 'integer');
				
				////.' and maillist_name.iduser='.$user_data['IdUser']
				
				$arr_maillist=$model['maillist_name']->select_a_row_where('where IdMaillist_name='.$_GET['idlist']);
				
				settype($arr_maillist['IdMaillist_name'], 'integer');
				
				if($arr_maillist['IdMaillist_name']>0)
				{
				
					echo '<h2>Borradores para la lista '.$arr_maillist['name'].'</h2>';
			
					echo $menu_hierarchy;
			
					$model['draft_email']->create_form();
					
					$model['draft_email']->forms['title']->label=$lang['common']['subject'];
					
					$model['draft_email']->forms['iduser']->form='HiddenForm';
					$model['draft_email']->forms['iduser']->parameters=array('iduser', '', $user_data['IdUser']);
					
					$model['draft_email']->forms['idlist']->form='HiddenForm';
					$model['draft_email']->forms['idlist']->parameters=array('idlist', '', $_GET['idlist']);
					
					$model['draft_email']->forms['message']->form='TextAreaBBForm';
				
					$admin=new GenerateAdminClass('draft_email');
					
					$admin->arr_fields=array('title');
					
					$admin->txt_list_new=$lang['maillist']['draft_email'];
					
					$admin->txt_add_new_item=$lang['maillist']['add_new_draft_email'];
					
					$admin->url_options=make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 7, 'idlist' => $_GET['idlist']));
					
					//$admin->where_sql='where iduser='.$user_data['IdUser'];
					
					$admin->simple_redirect=1;
					
					$admin->options_func='DraftOptionsList';
					
					$admin->no_search=1;
					
					$admin->where_sql='where idlist='.$_GET['idlist'];
					
					$admin->show();
					
				}
				
				echo $menu_hierarchy;
			
			break;
			
			case 8:
				
				settype($_GET['idlist'], 'integer');
				//.' and maillist_name.iduser='.$user_data['IdUser']
				$arr_maillist=$model['maillist_name']->select_a_row_where('where IdMaillist_name='.$_GET['idlist']);
				
				settype($arr_maillist['IdMaillist_name'], 'integer');
				
				if($arr_maillist['IdMaillist_name']>0)
				{
				
					echo '<h3>Añadir usuarios a '.$arr_maillist['name'].'</h3>';
				
					$arr_menus[0]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['maillists'], 'name_op' => 'op', 'params' => array('op' => 0));
	
					$arr_menus[1]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => $lang['maillist']['maillist'].': '.$arr_maillist['name'], 'name_op' => 'op', 'params' => array('op' => 1,  'IdMaillist_name' => $arr_maillist['IdMaillist_name']));
					
					$arr_menus[2]=array('module' => 'maillist/frontend', 'controller' => 'index', 'text' => 'Añadir usuario a lista de correo', 'name_op' => 'op', 'params' => array('op' => 8, 'idlist' => $arr_maillist['IdMaillist_name']));
					
					$menu_hierarchy='<p>'.menu_barr_hierarchy_control($arr_menus).'</p>';
					
					$model['middle_user_list']->create_form();
					
					$model['middle_user_list']->forms['iduser']->form='SelectModelForm';
					
					$model['middle_user_list']->forms['iduser']->parameters=array('iduser', '', '', 'user_list', 'iduser', $where='where user_list.iduser!='.$user_data['IdUser'].' and user_list.iduser>0');
					
					$model['middle_user_list']->forms['idmaillist_name']->form='HiddenForm';
					
					$model['middle_user_list']->forms['idmaillist_name']->parameters=array('idmaillist_name', '', $arr_maillist['IdMaillist_name']);
					
					$model['middle_user_list']->label='Usuario de lista de correo';
					
					echo $menu_hierarchy;
				
					$admin=new GenerateAdminClass('middle_user_list');
					
					$admin->arr_fields=array('iduser');
					
					$admin->no_search=1;
					
					$admin->url_options=make_fancy_url($base_url, 'maillist/frontend', 'index', 'Añadir usuario a lista de correo', array('op' => 8, 'idlist' => $arr_maillist['IdMaillist_name'] ));
					
					$admin->simple_redirect=1;
					
					$admin->txt_add_new_item='Añadir nuevo usuario a lista de correo';
					
					$admin->show();
				
					echo $menu_hierarchy;
				
				}
			
			break;
		
		}
		
	}
	else
	{
		/*$title_maillist=$lang['maillist']['without_permissions'];
	
		echo '<p>'.$lang['maillist']['without_permissions'].'</p>';*/
		
		$url_maillist=make_fancy_url($base_url, 'maillist/frontend', 'index', 'frontend', array());
		
		die(header('Location: '.make_fancy_url($base_url, 'user', 'index', 'login', array('register_page' => urlencode_redirect($url_maillist)), true ) ));
	
	}
	
	$content=ob_get_contents();
	
	ob_end_clean();

	echo load_view(array($title_maillist, $content), 'maillist/mailfrontend');
}

function MailOptionsList($url_options, $model_name, $id, $arr_row)
{

	global $model;

	?>
	<script language="javascript">
		function warning()
		{
			if(confirm('¿Quiere borrar la lista de correo?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	</script>
	<?php
	
	$url_options_edit=add_extra_fancy_url($url_options, array('op_edit' =>1, $model[$model_name]->idmodel => $id));
	$url_options_delete=add_extra_fancy_url($url_options, array('op_edit' =>2, $model[$model_name]->idmodel => $id));
	$url_options_send=add_extra_fancy_url($url_options, array('op'=> 2, 'idlist' => $id));
	$url_options_draft=add_extra_fancy_url($url_options, array('op'=> 7, 'idlist' => $id));
	
	$arr_link[]='<a href="'.$url_options_edit.'" id="edit_list" title="Editar lista de correo"><span>Editar lista de correo</span></a>';
	$arr_link[]='<a href="'.$url_options_draft.'" id="edit_draft" title="Envío desde borrador"><span>Envío desde borrador</span></a>';
	$arr_link[]='<a href="'.$url_options_send.'" id="send_message" title="Envío puntual"><span>Envío puntual</span></a>';
	$arr_link[]='<a href="'.$url_options_delete.'" onclick="javascript: if(warning()==false) { return false; }" id="delete_list" title="Eliminar"><span>Eliminar</span></a>';
	
	return $arr_link;

}

function MailNoOwnerOptionsList($url_options, $model_name, $id, $arr_row)
{

	global $model;

	?>
	<script language="javascript">
		function warning()
		{
			if(confirm('¿Quiere borrar la lista de correo?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	</script>
	<?php
	
	$url_options_edit=add_extra_fancy_url($url_options, array('op_edit' =>1, 'idlist' => $arr_row['idmaillist_name']));
	$url_options_delete=add_extra_fancy_url($url_options, array('op_edit' =>2, 'idlist' => $arr_row['idmaillist_name']));
	$url_options_send=add_extra_fancy_url($url_options, array('op'=> 2, 'idlist' => $arr_row['idmaillist_name']));
	$url_options_draft=add_extra_fancy_url($url_options, array('op'=> 7, 'idlist' => $arr_row['idmaillist_name']));
	
	//$arr_link[]='<a href="'.$url_options_edit.'" id="edit_list" title="Editar lista de correo"><span>Editar lista de correo</span></a>';
	$arr_link[]='<a href="'.$url_options_draft.'" id="edit_draft" title="Envío desde borrador"><span>Envío desde borrador</span></a>';
	$arr_link[]='<a href="'.$url_options_send.'" id="send_message" title="Envío puntual"><span>Envío puntual</span></a>';
	//$arr_link[]='<a href="'.$url_options_delete.'" onclick="javascript: if(warning()==false) { return false; }" id="delete_list" title="Eliminar"><span>Eliminar</span></a>';
	
	return $arr_link;

}

function DraftOptionsList($url_options, $model_name, $id, $arr_row)
{

	global $base_url;
	
	//http://localhost/phangodev/index.php/maillist/frontend/show/index/maillist/op/2/idlist/4
	settype($_GET['idlist'], 'integer');
	
	$arr_options[]='<a href="'.make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 2, 'idlist' => $_GET['idlist'], 'iddraft' => $arr_row['IdDraft_email'])).'">Usar este borrador para enviar mensaje a lista</a>';
	
	$arr_options_basic=BasicOptionsListModel($url_options, $model_name, $id);
	
	$arr_options=array_merge($arr_options, $arr_options_basic);
	
	return $arr_options;

}

function ListOptionsListModel($url_options, $model_name, $id, $arr_row)
{

	global $base_url, $user_data;
	
	//http://localhost/phangodev/index.php/maillist/frontend/show/index/maillist/op/2/idlist/4
	
	if($arr_row['iduser']==$user_data['IdUser'])
	{
	
		$arr_options[]='<a href="'.make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('op' => 8, 'idlist' => $id)).'">Añadir usuario a esta lista</a>';
	
	}
	
	$arr_options_basic=BasicOptionsListModel($url_options, $model_name, $id);
	
	$arr_options=array_merge($arr_options, $arr_options_basic);
	
	return $arr_options;

}


?>