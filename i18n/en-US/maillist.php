<?php

$lang['maillist']['add_email_list']='add_email_list';

$lang['maillist']['all_lists']='all_lists';

$lang['maillist']['send_mail_list']='send_mail_list';

$lang['maillist']['assign_users']='assign_users';

$lang['maillist']['maillist']='maillist';

$lang['maillist']['table_related']='table_related';

$lang['maillist']['field_related']='field_related';

$lang['maillist']['module_default']='module_default';

$lang['maillist']['edit_mail_list_ext']='edit_mail_list_ext';

$lang['maillist']['edit_mail_from_maillist']='edit_mail_from_maillist';

$lang['maillist']['email']='email';

$lang['maillist']['go_back_home_maillist']='go_back_home_maillist';

$lang['maillist']['send_owner_mail']='send_owner_mail';

$lang['maillist']['mail_type']='mail_type';

$lang['maillist']['mail_module']='mail_module';

$lang['maillist']['send_mail']='send_mail';

$lang['maillist']['mail_in_queue']='mail_in_queue';

$lang['maillist']['loading_daemon_mail']='loading_daemon_mail';

$lang['maillist']['error_daemon_no_loading']='error_daemon_no_loading';

$lang['maillist']['sending_mail_by_daemon']='sending_mail_by_daemon';

$lang['maillist']['daemon_sending_list_wait']='daemon_sending_list_wait';

$lang['maillist']['mass_emails']='mass_emails';

$lang['maillist']['proccess_mass_email']='proccess_mass_email';

$lang['maillist']['mass_email_processing_success']='mass_email_processing_success';

$lang['maillist']['mass_email_accepted']='mass_email_accepted';

$lang['maillist']['mass_email_rejected']='mass_email_rejected';

$lang['maillist']['mass_email_processing']='mass_email_processing';

$lang['maillist']['delete_mass_email']='delete_mass_email';

$lang['maillist']['mass_email_deleting_success']='mass_email_deleting_success';

$lang['maillist']['mass_email_deleting']='mass_email_deleting';

$lang['maillist']['user_list']='user_list';

$lang['maillist']['admin_emails']='admin_emails';

$lang['maillist']['add_mass_email']='add_mass_email';

$lang['maillist']['list']='list';

$lang['maillist']['welcome_subscription']='welcome_subscription';

$lang['maillist']['instructions_explain']='instructions_explain';

$lang['maillist']['unsubscribe_instructions']='unsubscribe_instructions';

$lang['maillist']['no_send_mail']='no_send_mail';

$lang['maillist']['send_mail_with_instructions']='send_mail_with_instructions';

$lang['maillist']['no_exists_mail']='no_exists_mail';

$lang['maillist']['delete_mail_success']='delete_mail_success';

$lang['maillist']['error_deleting_mail']='error_deleting_mail';

$lang['maillist']['delete_email_list']='delete_email_list';

$lang['maillist']['error_adding_email']='error_adding_email';

$lang['maillist']['email_exists']='email_exists';

$lang['maillist']['success_adding_email']='success_adding_email';

$lang['maillist']['success_adding_email_explain']='success_adding_email_explain';

$lang['maillist']['error_adding_email_explain']='error_adding_email_explain';

$lang['maillist']['no_module']='no_module';

$lang['maillist']['attachment']='attachment';

$lang['maillist']['email_in_lists']='email_in_lists';

$lang['maillist']['num_emails']='num_emails';

$lang['maillist']['num_emails_sended']='num_emails_sended';

$lang['maillist']['num_emails_sended_by_date']='num_emails_sended_by_date';

$lang['maillist']['email_sended_by_lists']='email_sended_by_lists';

$lang['maillist']['stats']='stats';

$lang['maillist']['choose_option_stat']='choose_option_stat';

$lang['maillist']['error_email_exists_in_db']='error_email_exists_in_db';

$lang['maillist']['signature']='signature';

$lang['maillist']['programmed']='programmed';

$lang['maillist']['activated']='activated';

$lang['maillist']['repeat']='repeat';

$lang['maillist']['maillists']='maillists';

$lang['maillist']['add_new_list']='add_new_list';

$lang['maillist']['company']='company';

$lang['maillist']['date_added']='date_added';

$lang['maillist']['job']='job';

$lang['maillist']['activation']='activation';

$lang['maillist']['email_subscription']='email_subscription';

$lang['maillist']['add_new_list_email']='add_new_list_email';

$lang['maillist']['csv_list']='csv_list';

$lang['maillist']['alphabetic_order']='alphabetic_order';

$lang['maillist']['alphabetic_order_inverse']='alphabetic_order_inverse';

$lang['maillist']['draft_list']='draft_list';

$lang['maillist']['draft_email']='draft_email';

$lang['maillist']['add_new_draft_email']='add_new_draft_email';

$lang['maillist']['without_permissions']='without_permissions';

$lang['maillist']['status_list']='status_list';

$lang['maillist']['process_emails']='process_emails';

$lang['maillist']['problem']='problem';

$lang['maillist']['cron_emails']='cron_emails';

$lang['maillist']['finished_emails']='finished_emails';

$lang['maillist']['no_killing_process']='no_killing_process';

$lang['maillist']['killing_process']='killing_process';

$lang['maillist']['killing_process_explain']='killing_process_explain';

$lang['maillist']['no_killing_process_explain']='no_killing_process_explain';

$lang['maillist']['unsubscribe_email_here']='unsubscribe_email_here';

$lang['maillist']['click_here']='click_here';

$lang['maillist']['case_break_num_id']='case_break_num_id';

$lang['maillist']['instructions']='instructions';

$lang['maillist']['Cargo en la empresa del dueño del email']='cargo-en-la-empresa-del-dueno-del-email';

$lang['maillist']['email_no_exists']='email_no_exists';

?>
