<?php

$lang['maillist']['add_email_list']='Añadir email a una de nuestras lista de correo';

$lang['maillist']['all_lists']='Todas las listas';

$lang['maillist']['send_mail_list']='Enviar mensaje a lista de correo';

$lang['maillist']['assign_users']='Asignar usuarios a listas de correo';

$lang['maillist']['maillist']='Lista de correo';

$lang['maillist']['table_related']='Tabla relacionada';

$lang['maillist']['field_related']='Campo relacionado';

$lang['maillist']['module_default']='Módulo por defecto';

$lang['maillist']['edit_mail_list_ext']='Editar listas de email externas';

$lang['maillist']['edit_mail_from_maillist']='Editar emails de esta lista';

$lang['maillist']['email']='Email';

$lang['maillist']['go_back_home_maillist']='Regresar a listas de correo';

$lang['maillist']['send_owner_mail']='Enviar SÓLO al remitente (para pruebas)';

$lang['maillist']['mail_type']='Tipo de email';

$lang['maillist']['mail_module']='Módulo a utilizar para crear el email';

$lang['maillist']['send_mail']='Enviar email';

$lang['maillist']['mail_in_queue']='Email en cola';

$lang['maillist']['loading_daemon_mail']='Enviando los e-mails';

$lang['maillist']['error_daemon_no_loading']='Error al cargar el motor de envío de email';

$lang['maillist']['sending_mail_by_daemon']='El motor de envío está enviado correctamente los e-mails, cuando finalice le enviará un mensaje a su correo electrónico';

$lang['maillist']['daemon_sending_list_wait']='El motor de envío está enviando una lista anterior, por favor, espere';

$lang['maillist']['mass_emails']='Fichero de Emails (en el fichero, ponga un email por linea)';

$lang['maillist']['proccess_mass_email']='Procesar direcciones de email';

$lang['maillist']['mass_email_processing_success']='Se procesaron los emails con éxito';

$lang['maillist']['mass_email_accepted']='Emails aceptados';

$lang['maillist']['mass_email_rejected']='Emails rechazados(por estar mal escritos y si está añadiendo nuevos emails, porque ya existen en la base de datos)';

$lang['maillist']['mass_email_processing']='Procesador de fichero con emails';

$lang['maillist']['delete_mass_email']='Eliminar emails de forma masiva';

$lang['maillist']['mass_email_deleting_success']='Se procesó el fichero y se borraron emails con éxito';

$lang['maillist']['mass_email_deleting']='Borrando emails';

$lang['maillist']['user_list']='Usuarios con permisos para administrar listas';

$lang['maillist']['admin_emails']='Administrar emails de la lista';

$lang['maillist']['add_mass_email']='Añadir emails de forma masiva';

$lang['maillist']['list']='Lista de correo';

$lang['maillist']['welcome_subscription']='Usted nos ha pedido dar de baja su correo de nuestro servicio.';

$lang['maillist']['instructions_explain']='Para finalizar el proceso, por favor, copie y pegue el enlace que le suministramos en este email, en su navegador. Una vez hecho eso, su email será dado de baja definitivamente.';

$lang['maillist']['unsubscribe_instructions']='Darse de baja en nuestra lista de correo';

$lang['maillist']['no_send_mail']='No se puede enviar el email, por favor envíenos un correo a la siguiente dirección:';

$lang['maillist']['send_mail_with_instructions']='Hemos anotado su petición y le hemos enviado un mensaje a su correo dándole instrucciones para dar la baja definitiva de su correo en esta lista.';

$lang['maillist']['no_exists_mail']='No existe el email';

$lang['maillist']['delete_mail_success']='Se eliminó el email con éxito de nuestra base de datos. Gracias por utilizar el servicio.';

$lang['maillist']['error_deleting_mail']='Error borrando email, por favor, envíe un correo a esta dirección para dar de baja la dirección de forma manual';

$lang['maillist']['delete_email_list']='Borrar email de la listas de correo';

$lang['maillist']['error_adding_email']='Error añadiendo email';

$lang['maillist']['email_exists']='El Email ya existe en la base de datos en la lista escogida.';

$lang['maillist']['success_adding_email']='Éxito añadiendo email';

$lang['maillist']['success_adding_email_explain']='Éxito añadiendo el nuevo email a las listas escogidas.';

$lang['maillist']['error_adding_email_explain']='Error: no se pudo introducir el email en la base de datos. ¿Está en un formato correcto?.';

$lang['maillist']['no_module']='No utilizar ningún módulo';

$lang['maillist']['attachment']='Fichero adjunto';

$lang['maillist']['email_in_lists']='Número de emails por listas';

$lang['maillist']['num_emails']='Número de emails';

$lang['maillist']['num_emails_sended']='Número de emails enviados';

$lang['maillist']['num_emails_sended_by_date']='Número de emails enviados por fechas';

$lang['maillist']['email_sended_by_lists']='Número de emails enviados por listas';

$lang['maillist']['stats']='Estadísticas';

$lang['maillist']['choose_option_stat']='Elija estadística';

$lang['maillist']['error_email_exists_in_db']='Error, el email ya existe en la lista.';

$lang['maillist']['signature']='introducir la firma que desea incluir a esta lista de destinatarios. Esta firma quedará debajo del contenido de su correo que editará posteriormente a la creación de esta lista de destinatarios';

$lang['maillist']['programmed']='Fecha de envío programada';

$lang['maillist']['activated']='¿Activar envío programado?';

$lang['maillist']['repeat']='Repetir el envío cada cierto número de días';

$lang['maillist']['maillists']='Listas de correo';

$lang['maillist']['add_new_list']='Añadir nueva lista de correo';

$lang['maillist']['company']='Empresa';

$lang['maillist']['date_added']='Fecha de adición a la base de datos';

$lang['maillist']['job']='Cargo en la empresa del dueño del email';

$lang['maillist']['activation']='¿Email activado?';

$lang['maillist']['email_subscription']='Emails suscritos a esta lista';

$lang['maillist']['add_new_list_email']='Añadir nuevo email a la lista';

$lang['maillist']['csv_list']='Descargar CSV para Excel';

$lang['maillist']['alphabetic_order']='De la A a la Z';

$lang['maillist']['alphabetic_order_inverse']='De la Z a la A';

$lang['maillist']['draft_list']='Enviar desde borrador';

$lang['maillist']['draft_email']='Borradores disponibles';

$lang['maillist']['add_new_draft_email']='Añadir nuevo borrador';

$lang['maillist']['without_permissions']='No tiene permisos para usar esta aplicación';

$lang['maillist']['status_list']='Listado de procesos de email';

$lang['maillist']['process_emails']='Listas de correo en proceso';

$lang['maillist']['problem']='¿Algún problema?';

$lang['maillist']['cron_emails']='Listas de correo programadas';

$lang['maillist']['finished_emails']='Listas de correo enviadas y finalizadas';

$lang['maillist']['no_killing_process']='No se terminó el proceso de envío de emails';

$lang['maillist']['killing_process']='Se terminó el proceso de envío de emails';

$lang['maillist']['killing_process_explain']='Se ha terminado el proceso de envío de emails por orden del usuario. Se ha marcado el envío como fallido en la base de datos.';

$lang['maillist']['no_killing_process_explain']='No se ha terminado el proceso de envío de emails por orden del usuario, para pararlo si está activo (todo indica que no), deberá de hacerlo manualmente, seguramente como root. Se ha marcado el envío como fallido en la base de datos.';

$lang['maillist']['unsubscribe_email_here']='Puede dar de baja su correo electrónico en este enlace: ';

$lang['maillist']['click_here']='pulse aquí';

$lang['maillist']['case_break_num_id']='En caso de que necesite contactar con nosotros en relación a este email, su número de id. es';

$lang['maillist']['instructions']='instructions';

$lang['maillist']['Cargo en la empresa del dueño del email']='cargo-en-la-empresa-del-dueno-del-email';

?>
