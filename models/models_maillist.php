<?php
load_model('user');
load_libraries(array('i18n_fields'));

//Lists
//Bug no delete emails.

class MailCharField extends Charfield {

	function show_formatted($value)
	{
	
		global $base_url;
	
		$arr_func=func_get_args();
		
		settype($arr_func[1], 'integer');
	
		$id=$arr_func[1];
	
		return '<a href="'.make_fancy_url($base_url, 'maillist/frontend', 'index', 'maillist', array('IdMaillist_name' => $id, 'op' => 1)).'">'.$value.'</a>';
	
	}

}

$model['maillist_name']=new Webmodel('maillist_name');

$model['maillist_name']->components['name']=new MailCharField(255);
$model['maillist_name']->components['name']->required=1;
$model['maillist_name']->components['table_related']=new CharField(255);
$model['maillist_name']->components['field_related']=new CharField(255);
$model['maillist_name']->components['module_default']=new ChoiceField(255, 'string');
$model['maillist_name']->set_component('iduser', 'ForeignKeyField', array('user'), 0);
$model['maillist_name']->set_component('signature', 'TextHTMLField', array(), 0);
$model['maillist_name']->set_component('programmed', 'DateField', array(), 0);
$model['maillist_name']->components['programmed']->set_default_time=1;
$model['maillist_name']->set_component('activated', 'BooleanField', array(), 0);
$model['maillist_name']->set_component('repeat', 'IntegerField', array(), 0);

$model['middle_user_list']=new Webmodel('middle_user_list');

$model['middle_user_list']->set_component('iduser', 'ForeignKeyField', array('user'), 1);

$model['middle_user_list']->components['iduser']->name_field_to_field='private_nick';

$model['middle_user_list']->set_component('idmaillist_name', 'ForeignKeyField', array('maillist_name'), 1);

class maillist extends Webmodel {

	function __construct()
	{

		parent::__construct("maillist");

	}	
	
}

$model['maillist']=new maillist();

$model['maillist']->components['token']=new CharField(255);
$model['maillist']->components['token']->required=1;

$model['maillist']->components['title']=new CharField(255);
$model['maillist']->components['title']->required=1;

$model['maillist']->components['message']=new TextHTMLField();
$model['maillist']->components['message']->required=1;

$model['maillist']->components['iduser']=new IntegerField(11);
$model['maillist']->components['iduser']->required=1;

$model['maillist']->components['type']=new IntegerField(2);

//$model['maillist']->components['idlist']=new IntegerField(11);

$model['maillist']->set_component('idlist', 'ForeignKeyField', array('maillist_name'), 1);

$model['maillist']->components['module']=new ChoiceField($size=255, $type='string', $arr_values=array(), $default_value='');

$model['maillist']->components['finished']=new BooleanField();

$model['maillist']->set_component('problem', 'BooleanField', array(), 0);

$model['maillist']->set_component('trial', 'BooleanField', array(), 0);

$model['maillist']->set_component('attachment', 'SerializeField', array(), 0);

$model['maillist']->set_component('unique_id', 'CharField', array(255), 1);

class sendmail extends Webmodel {

	function __construct()
	{

		parent::__construct("sendmail");

	}	
	
}

$model['sendmail']=new sendmail();

$model['sendmail']->components['num_emails']=new IntegerField(255);
$model['sendmail']->components['num_emails']->required=1;

$model['sendmail']->components['num_email_sended']=new IntegerField(255);

$model['sendmail']->components['idmaillist']=new IntegerField(255);

$model['sendmail']->components['idmaillist_name']=new IntegerField(255);

$model['sendmail']->set_component('pid', 'IntegerField', array(11), 0);

$model['sendmail']->components['finished']=new BooleanField();

$model['sendmail']->components['date']=new DateField();

class maillist_email extends Webmodel {

	function __construct()
	{

		parent::__construct("maillist_email");

	}	
	
	function check_exists_email($email, $idpost=0, $idlist=0)
	{
	
		global $lang;
	
		$email=$this->components['email']->check($email);
		
		$where_sql='where email="'.$email.'"';
		
		if($idpost>0)
		{
		
			$where_sql.=' and IdMaillist_email!='.$idpost;
		
		}
		
		
		$where_sql.=' and idlist='.$idlist;
		
		$num_email=$this->select_count($where_sql, 'IdMaillist_email');
		
		if($num_email>0)
		{
			$this->components['email']->std_error=$lang['maillist']['error_email_exists_in_db'];
			return '';
		
		}
		
		return $email;
	
	}
	
	function insert($post)
	{	
	
		global $lang;
	
		settype($post['idlist'], 'integer');
		
		if($this->check_exists_email($post['email'], 0, $post['idlist'])!='')
		{
		
			return parent::insert($post);
		
		}
		else
		{
			$this->std_error.=$lang['maillist']['error_email_exists_in_db'];
			return 0;
		
		}
	
	}
	
	function update($post, $conditions="", $no_check=0)
	{	
	
		global $lang;
	
		settype($post['IdMaillist_email'], 'integer');
		
		$yes_update=1;
		
		if(!$no_check)
		{
			if($this->check_exists_email($post['email'], $post['IdMaillist_email'])=='')
			{
			
				$yes_update=0;
			
			}
			
		}
		
		if($yes_update)
		{
		
			return parent::update($post, $conditions);
		
		}
		else
		{
			$this->std_error.=$lang['maillist']['error_email_exists_in_db'];
			return 0;
		
		}
	
	}
	
}

$model['maillist_email']=new maillist_email();

$model['maillist_email']->components['email']=new EmailField();
$model['maillist_email']->components['email']->required=1;

$model['maillist_email']->components['name']=new CharField(255);
$model['maillist_email']->components['name']->required=0;

$model['maillist_email']->components['company']=new CharField(255);
$model['maillist_email']->components['company']->required=0;

$model['maillist_email']->components['job']=new CharField(255);
$model['maillist_email']->components['job']->required=0;

$model['maillist_email']->components['date']=new DateField();
$model['maillist_email']->components['date']->required=0;

$model['maillist_email']->components['activation']=new BooleanField();
$model['maillist_email']->components['activation']->required=0;
$model['maillist_email']->components['activation']->default_value=1;

//$model['maillist_email']->components['idlist']=new ForeignKeyField('maillist_name');
$model['maillist_email']->set_component('idlist', 'ForeignKeyField', array('maillist_name'));

$model['maillist_email']->components['because_disabled']=new TextField();
$model['maillist_email']->components['because_disabled']->required=0;

$model['user_list']=new Webmodel('user_list');

$model['user_list']->set_component('iduser', 'ForeignKeyField', array('user'), 1);

$model['user_list']->components['iduser']->name_field_to_field='private_nick';

$model['config_maillist']=new Webmodel('config_maillist');

$model['config_maillist']->set_component('name_app', 'I18nField', array(new CharField(255)), 0);

$model['unsubscribe_maillist']=new Webmodel('unsubscribe_maillist');

$model['unsubscribe_maillist']->set_component('email', 'CharField', array(255), 1);

$model['unsubscribe_maillist']->set_component('delete_email', 'BooleanField', array(), 0);

$model['unsubscribe_maillist']->set_component('date', 'DateField', array(), 0);

$model['unsubscribe_maillist']->set_component('token', 'CharField', array(255), 1);

$model['unsubscribe_maillist']->set_component('idlist', 'ForeignKeyField', array('maillist_name'), 0);

$model['draft_email']=new Webmodel('draft_email');

$model['draft_email']->set_component('title', 'CharField', array(255), 1);

$model['draft_email']->set_component('message', 'TextHTMLField', array(), 1);

$model['draft_email']->set_component('iduser', 'ForeignKeyField', array('user'), 1);
$model['draft_email']->set_component('idlist', 'ForeignKeyField', array('maillist_name'), 1);

//$model['unsubscribe_maillist']->['delete_email']->default_value=0;

$arr_module_insert['maillist']=array('name' => 'maillist', 'admin' => 1, 'admin_script' => array('maillist', 'maillist'), 'load_module' => '', 'order_module' => 8, 'required' => 0);

?>