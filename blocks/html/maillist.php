<?php

global $lang, $model, $base_url;

load_model('maillist');
load_lang('maillist');

echo '<form action="'.make_fancy_url($base_url, 'maillist', 'addmail', 'addmail', array()).'" method="post">';

set_csrf_key();

echo '<p align="center">'.$lang['maillist']['add_email_list'].'</p>';

echo '<p align="center">'.$lang['common']['email'].'<br />'.TextForm('email', '', '').'</p>';

$query=$model['maillist_name']->select('', array('IdMaillist_name', 'name'));

$arr_list=array(0, $lang['maillist']['all_lists'], 0);

while(list($idlist, $name)=webtsys_fetch_row($query))
{

	$arr_list[]=$name;
	$arr_list[]=$idlist;

}

echo '<p align="center">'.$lang['common']['list'].'<br />'.SelectForm('idlist', '', $arr_list).'</p>';

echo '<p align="center"><input type="submit" value="'.$lang['common']['send'].'"/></p>';

echo '</form>';

?>